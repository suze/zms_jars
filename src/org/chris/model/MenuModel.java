package org.chris.model;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;

/**
 * 菜单类
 * @author Chris Suk
 * @date 2014-4-4 上午09:12:46
 */
@Table("menu")
public class MenuModel {
	@Id
	@Column
	private int id;
	@Column("pid")
	private int pId;
	@Column
	private String name;
	@Column
	private String url;
	@Column("order_num")
	private int orderNum;
	
	private boolean isParent=false;
	private boolean open=true;
	private String title;
	
	public MenuModel(){}
	public MenuModel(int id,int pId,String name,String url,int orderNum){
		this.id = id;
		this.pId = pId;
		this.name = name;
		this.url = url;
		this.orderNum = orderNum;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public int getPId() {
		return pId;
	}
	public void setPId(int id) {
		pId = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	public int getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}
	public boolean isParent() {
		return isParent;
	}
	public void setParent(boolean isParent) {
		this.isParent = isParent;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}

	public String getTitle() {
		return this.name;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
