package org.chris.model;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.One;
import org.nutz.dao.entity.annotation.Table;
/**
 * 商品实体类
 * @author Chris Suk
 * @date 2014-5-23 下午10:37:05
 */
@Table("product")
public class ProductModel {
	@Id
	@Column
	private int id;
	@Column
	private String name;
	@Column
	private String model;
	@Column
	private double price;
	@Column
	private String remark;
	@Column
	private double createBy;
	@Column
	private String createDate;
	@Column
	private int brandId;
	@Column
	private int pcId;
	@Column
	private int supplierId;
	@One(target = BrandModel.class, field = "brandId")
	private BrandModel brand;
	@One(target = ProductClass.class, field = "pcId")
	private ProductClass productClass;
	@One(target = SupplierModel.class, field = "supplierId")
	private SupplierModel supplier;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public double getCreateBy() {
		return createBy;
	}

	public void setCreateBy(double createBy) {
		this.createBy = createBy;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public int getBrandId() {
		return brandId;
	}

	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}

	public int getPcId() {
		return pcId;
	}

	public void setPcId(int pcId) {
		this.pcId = pcId;
	}

	public int getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(int supplierId) {
		this.supplierId = supplierId;
	}

	public BrandModel getBrand() {
		return brand;
	}

	public void setBrand(BrandModel brand) {
		this.brand = brand;
	}

	public ProductClass getProductClass() {
		return productClass;
	}

	public void setProductClass(ProductClass productClass) {
		this.productClass = productClass;
	}

	public SupplierModel getSupplier() {
		return supplier;
	}

	public void setSupplier(SupplierModel supplier) {
		this.supplier = supplier;
	}

}
