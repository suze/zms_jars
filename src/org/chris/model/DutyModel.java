package org.chris.model;


import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;
/**
 * 职务实体类
 * @author Chris Suk
 * @date 2014-4-24 下午08:39:00
 */
@Table("duty")
public class DutyModel {
	@Column
	@Id
	private int id;
	@Column
	private String name;
	@Column("order_num")
	private int orderNum;
	
	public DutyModel(){}
	public DutyModel(int id,String name,int orderNum){
		this.id =  id;
		this.name = name;
		this.orderNum=orderNum;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}
	
	
}
