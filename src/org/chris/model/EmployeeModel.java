package org.chris.model;

import java.util.Date;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.One;
import org.nutz.dao.entity.annotation.Table;

@Table("employee")
public class EmployeeModel {
	@Id
	@Column
	private int id;
	@Column
	private String name;
	@Column("duty_id")
	private int dutyId;
	@Column
	private String gender;
	@Column
	private Date birthday;
	@Column
	private Date firedate;
	@Column
	private Date hiredate;
	@Column
	private String idcard;
	@Column
	private String address;
	@Column
	private String phone;
	@Column
	private String email;
	@Column("create_by")
	private int createBy;
	@Column("create_date")
	private Date createDate;
	@One(target=DutyModel.class,field="dutyId")
	private DutyModel duty;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDutyId() {
		return dutyId;
	}
	public void setDutyId(int dutyId) {
		this.dutyId = dutyId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Date getFiredate() {
		return firedate;
	}
	public void setFiredate(Date firedate) {
		this.firedate = firedate;
	}
	public Date getHiredate() {
		return hiredate;
	}
	public void setHiredate(Date hiredate) {
		this.hiredate = hiredate;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getCreateBy() {
		return createBy;
	}
	public void setCreateBy(int createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public DutyModel getDuty() {
		return duty;
	}
	public void setDuty(DutyModel duty) {
		this.duty = duty;
	}
}
