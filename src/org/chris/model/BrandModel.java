package org.chris.model;

import java.util.Date;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;
/**
 * 品牌实体类
 * @author Chris Suk
 * @date 2014-4-26 下午04:07:53
 */
@Table("brand")
public class BrandModel {
	@Column
	@Id
	private int id;
	@Column
	private String name;
	@Column("order_num")
	private int orderNum;
	@Column("create_by")
	private String createBy;
	@Column("create_date")
	private Date createDate;
	
	public BrandModel(){}
	public BrandModel(int id,String name,int orderNum,String createBy,Date createDate){
		this.id = id;
		this.name = name;
		this.orderNum = orderNum;
		this.createBy=createBy;
		this.createDate=createDate;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(int orderNum) {
		this.orderNum = orderNum;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
