package org.chris.module;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.CustomerModel;
import org.chris.service.impl.CustomerServiceImpl;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
/**
 * 客户模块
 * @author Chris Suk
 * @date 2014-5-21 下午10:59:46
 */
@IocBean
public class CustomerModule {
	@Inject
	private CustomerServiceImpl customerService;
	
	@At("/customer/save")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> save(@Param("::model.") CustomerModel model) {
		return this.customerService.save(model);
	}
	@At("/customer/delete")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> delete(HttpServletRequest request) {
		String[] ids = request.getParameter("ids").split(",");
		return this.customerService.delete(ids);
	}
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/customer/init")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> init(HttpServletRequest request,int rows,int page) {
		Map<String, Object> map = this.customerService.query(request,rows,page);
		return map;
	}
	/**
	 * 编辑
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/customer/edit")
	@Ok("jsp:/module/basic/basic_customer_edit.jsp")
	// 返回形式是jsp
	public void edit(HttpServletRequest request,int id,String action) {
		CustomerModel model=this.customerService.find(id);
		request.setAttribute("model", model);
		request.setAttribute("action", action);
	}
	public CustomerServiceImpl getCustomerService() {
		return customerService;
	}
	public void setCustomerService(CustomerServiceImpl customerService) {
		this.customerService = customerService;
	}
	
}
