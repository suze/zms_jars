package org.chris.module;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.DutyModel;
import org.chris.service.DutyService;
import org.chris.utils.StringUtils;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

@IocBean()
public class DutyModule {
	@Inject
	private DutyService dutyService;
	@At("/duty/save")
	@Ok("json")
	// 返回形式是jsp
	public void save(HttpServletRequest request) {
		try {
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String orderNum = request.getParameter("orderNum");
			DutyModel model = new DutyModel(StringUtils.toInt(id),name,StringUtils.toInt(orderNum));
			this.dutyService.save(model);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}
	@At("/duty/delete")
	@Ok("json")
	// 返回形式是jsp
	public void delete(HttpServletRequest request) {
		try {
			String[] ids = request.getParameter("ids").split(",");
			this.dutyService.delete(ids);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 * @param ioc
	 */
	@At("/duty/init")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> init(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<DutyModel> list = this.dutyService.init();
			map.put("total", String.valueOf(list.size()));
			map.put("rows", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
}
