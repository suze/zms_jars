package org.chris.module;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.chris.model.MenuModel;
import org.chris.model.UserModel;
import org.chris.service.MenuService;
import org.chris.service.UserService;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
/**
 * 登录
 * @author Chris Suk
 * @date 2014-5-13 下午09:16:28
 */
@IocBean
public class LoginModule { 
	@Inject
	private UserService userService;
	@Inject
	private MenuService menuService;
	/**
	 * 登录验证
	 * @author Chris Suk
	 * @date 2014-4-4 上午11:43:11
	 * @param username
	 * @param password
	 * @param ioc
	 * @param response
	 * @param session
	 * @return
	 */
	@At("/login")
	@Ok("json")
	public Map<String, String> login(@Param("username")String username, @Param("password")String password, HttpSession session) {
		Map<String, String> map = new HashMap<String, String>();
		try {
			UserModel user=userService.check(username, password);
			if(null!=user){
				map.put("type", "0");
				map.put("msg", "登录成功");
				session.setAttribute("user", user);
			}else{
				map.put("type", "1");
				map.put("msg", "用户名或密码错误");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	/**
	 * 加载菜单
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/login/menu/init")
	@Ok("json")
	// 返回形式是jsp
	public List<MenuModel> init(HttpServletRequest request) {
		List<MenuModel> list = new ArrayList<MenuModel>();
		try {
			list = this.menuService.init();
			MenuModel root = new MenuModel();
			root.setName("系统菜单");
			root.setParent(true);
			list.add(root);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
