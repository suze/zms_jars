package org.chris.module;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.UserModel;
import org.chris.service.impl.UserServiceImpl;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
/**
 * 用户处理模块
 * @author Chris Suk
 * @date 2014-5-14 下午09:33:02
 */
@IocBean
public class UserModule {
	@Inject
	private UserServiceImpl userService;
	
	@At("/user/save")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> save(@Param("::model.") UserModel model) {
		return this.userService.save(model);
	}
	@At("/user/delete")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> delete(HttpServletRequest request) {
		String[] ids = request.getParameter("ids").split(",");
		return this.userService.delete(ids);
	}
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/user/init")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> init(HttpServletRequest request,int rows,int page) {
		Map<String, Object> map = this.userService.query(request,rows,page);
		return map;
	}
	/**
	 * 编辑
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/user/edit")
	@Ok("jsp:/module/system/sys_user_edit.jsp")
	// 返回形式是jsp
	public void edit(HttpServletRequest request,int id,String action) {
		UserModel model=this.userService.find(id);
		request.setAttribute("model", model);
		request.setAttribute("action", action);
	}
}
