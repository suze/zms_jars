package org.chris.module;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.RoleModel;
import org.chris.service.impl.RoleServiceImpl;
import org.chris.utils.StringUtils;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
@IocBean
public class RoleModule {
	@Inject
	private RoleServiceImpl roleService;
	@At("/role/save")
	@Ok("json")
	// 返回形式是jsp
	public void save(HttpServletRequest request) {
		try {
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			RoleModel model = new RoleModel(StringUtils.toInt(id),name);
			this.roleService.save(model);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}
	@At("/role/delete")
	@Ok("json")
	// 返回形式是jsp
	public void delete(HttpServletRequest request) {
		try {
			String id = request.getParameter("id");
			this.roleService.delete(StringUtils.toInt(id));
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/role/init")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> init(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<RoleModel> list = this.roleService.init();
			map.put("total", String.valueOf(list.size()));
			map.put("rows", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
}
