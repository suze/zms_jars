package org.chris.module;

import org.nutz.mvc.annotation.Fail;
import org.nutz.mvc.annotation.IocBy;
import org.nutz.mvc.annotation.Modules;
import org.nutz.mvc.ioc.provider.ComboIocProvider;

@Modules(scanPackage=true)  
@Fail("json")  
@IocBy(type=ComboIocProvider.class, 
	args={
	"*org.nutz.ioc.loader.annotation.AnnotationIocLoader",
	"org.chris",
	"*org.nutz.ioc.loader.json.JsonLoader",
	"org/chris/config/dataSource.js"
	}) 
public class MainModule {

}
