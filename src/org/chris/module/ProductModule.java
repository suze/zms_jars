package org.chris.module;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.ProductModel;
import org.chris.service.impl.ProductServiceImpl;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
/**
 * 商品模块
 * @author Chris Suk
 * @date 2014-5-23 下午10:40:44
 */
@IocBean
public class ProductModule {
	@Inject
	private ProductServiceImpl productService;

	@At("/product/save")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> save(@Param("::model.") ProductModel model) {
		return this.productService.save(model);
	}
	@At("/product/delete")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> delete(HttpServletRequest request) {
		String[] ids = request.getParameter("ids").split(",");
		return this.productService.delete(ids);
	}
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/product/init")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> init(HttpServletRequest request,int rows,int page) {
		Map<String, Object> map = this.productService.query(request,rows,page);
		return map;
	}
	/**
	 * 编辑
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/product/edit")
	@Ok("jsp:/module/product/product_edit.jsp")
	// 返回形式是jsp
	public void edit(HttpServletRequest request,int id) {
		ProductModel model=this.productService.fetch(id);
		request.setAttribute("model", model);
	}
	
}
