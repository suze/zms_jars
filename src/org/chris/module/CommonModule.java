package org.chris.module;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.service.BrandService;
import org.chris.service.DutyService;
import org.chris.service.ProductClassService;
import org.chris.service.SupplierService;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

/**
 * 通用模块
 * @author Chris Suk
 * @date 2014-5-11 下午08:15:56
 */
@IocBean
public class CommonModule {
	/**
	 * 职务
	 */
	@Inject
	private DutyService dutyService;
	/**
	 * 品牌
	 */
	@Inject
	private BrandService brandService;
	/**
	 * 商品分类
	 */
	@Inject
	private ProductClassService productClassService;
	/**
	 * 供应商
	 */
	@Inject
	private SupplierService supplierService;
	/**
	 * 加载基础数据，本方法主要提供前端Combox数据接口
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/common/init")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> init(HttpServletRequest request,String type) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if(type.equals("brand")){
				map.put("rows", this.brandService.init());
				map.put("total", this.brandService.init().size());
			}else if(type.equals("duty")){
				map.put("rows", this.dutyService.init());
				map.put("total", this.dutyService.init().size());
			}else if(type.equals("productclass")){
				map.put("rows", this.productClassService.init());
				map.put("total", this.productClassService.init().size());
			}else if(type.equals("supplier")){
				map.put("rows", this.supplierService.init());
				map.put("total", this.supplierService.init().size());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
}
