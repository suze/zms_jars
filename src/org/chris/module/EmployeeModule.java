package org.chris.module;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.EmployeeModel;
import org.chris.service.impl.EmployeeServiceImpl;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;

@IocBean
public class EmployeeModule {
	@Inject
	private EmployeeServiceImpl employeeService;
	@At("/employee/save")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> save(@Param("::model.") EmployeeModel model) {
		return this.employeeService.save(model);
	}
	@At("/employee/delete")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> delete(HttpServletRequest request) {
		String[] ids = request.getParameter("ids").split(",");
		return this.employeeService.delete(ids);
	}
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/employee/init")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> init(HttpServletRequest request,int rows,int page) {
		Map<String, Object> map = this.employeeService.query(request,rows,page);
		return map;
	}
	/**
	 * 编辑
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/employee/edit")
	@Ok("jsp:/module/system/sys_employee_edit.jsp")
	// 返回形式是jsp
	public void edit(HttpServletRequest request,int id) {
		EmployeeModel model=this.employeeService.fetch(id);
		request.setAttribute("model", model);
	}
}
