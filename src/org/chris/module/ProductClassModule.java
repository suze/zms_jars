package org.chris.module;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.ProductClass;
import org.chris.service.ProductClassService;
import org.chris.utils.DateUtils;
import org.chris.utils.StringUtils;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
@IocBean
public class ProductClassModule {
	@Inject
	private ProductClassService productClassService;
	@At("/productclass/save")
	@Ok("json")
	// 返回形式是jsp
	public void save(HttpServletRequest request) {
		try {
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String no = request.getParameter("no");
			String orderNum = request.getParameter("orderNum");
			ProductClass model = new ProductClass(StringUtils.toInt(id),name,no,StringUtils.toInt(orderNum),0,DateUtils.getNow());
			this.productClassService.save(model);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}
	@At("/productclass/delete")
	@Ok("json")
	// 返回形式是jsp
	public void delete(HttpServletRequest request) {
		try {
			String[] ids = request.getParameter("ids").split(",");
			this.productClassService.delete(ids);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/productclass/init")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> init(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<ProductClass> list = this.productClassService.init();
			map.put("total", String.valueOf(list.size()));
			map.put("rows", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
}
