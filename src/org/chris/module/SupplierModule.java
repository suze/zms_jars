package org.chris.module;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.SupplierModel;
import org.chris.service.impl.SupplierServiceImpl;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;
import org.nutz.mvc.annotation.Param;
/**
 * 供应商模块
 * @author Chris Suk
 * @date 2014-5-21 下午11:00:03
 */
@IocBean
public class SupplierModule {
	@Inject
	private SupplierServiceImpl supplierService;
	
	@At("/supplier/save")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> save(@Param("::model.") SupplierModel model) {
		return this.supplierService.save(model);
	}
	@At("/supplier/delete")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> delete(HttpServletRequest request) {
		String[] ids = request.getParameter("ids").split(",");
		return this.supplierService.delete(ids);
	}
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/supplier/init")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> init(HttpServletRequest request,int rows,int page) {
		Map<String, Object> map = this.supplierService.query(request,rows,page);
		return map;
	}
	/**
	 * 编辑
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 */
	@At("/supplier/edit")
	@Ok("jsp:/module/basic/basic_supplier_edit.jsp")
	// 返回形式是jsp
	public void edit(HttpServletRequest request,int id,String action) {
		SupplierModel model=this.supplierService.find(id);
		request.setAttribute("model", model);
		request.setAttribute("action", action);
	}
	public SupplierServiceImpl getSupplierService() {
		return supplierService;
	}
	public void setSupplierService(SupplierServiceImpl supplierService) {
		this.supplierService = supplierService;
	}
	
}
