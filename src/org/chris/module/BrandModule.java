package org.chris.module;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.BrandModel;
import org.chris.service.impl.BrandServiceImpl;
import org.chris.utils.DateUtils;
import org.chris.utils.StringUtils;
import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

@IocBean()
public class BrandModule {
	@Inject
	private BrandServiceImpl brandService;
	@At("/brand/save")
	@Ok("json")
	// 返回形式是jsp
	public void save(HttpServletRequest request) {
		try {
			String id = request.getParameter("id");
			String name = request.getParameter("name");
			String orderNum = request.getParameter("orderNum");
			BrandModel model = new BrandModel(StringUtils.toInt(id),name,StringUtils.toInt(orderNum),"",DateUtils.getNow());
			this.brandService.save(model);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}
	@At("/brand/delete")
	@Ok("json")
	// 返回形式是jsp
	public void delete(HttpServletRequest request) {
		try {
			String[] ids = request.getParameter("ids").split(",");
			this.brandService.delete(ids);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 加载菜单
	 * @author Chris Suk
	 * @date 2014-4-22 下午10:13:57
	 * @param request
	 * @param ioc
	 */
	@At("/brand/init")
	@Ok("json")
	// 返回形式是jsp
	public Map<String, Object> init(HttpServletRequest request) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<BrandModel> list = this.brandService.init();
			map.put("total", String.valueOf(list.size()));
			map.put("rows", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
}
