package org.chris.utils;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class IoUtils {

	
	public static void close(InputStream is){
		try {
			if(is != null){
				is.close();
			}
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	public static void close(OutputStream os){
		try {
			if(os !=null){
				os.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			//throw new RuntimeException(e.getMessage());
		}
	}
	
	public static void flush(OutputStream os){
		try {
			if(os != null){
				os.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
