package org.chris.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 * 验证
 * @description  
 * @author Chris Suk
 * @date 2012-11-12 上午10:12:30
 */
public class Validate {
	public static void validate(HttpServletRequest request,HttpServletResponse response){
		try {
			HttpSession session = request.getSession();
			if(null==session.getAttribute("sessionUser")){
				response.sendRedirect(request.getContextPath()+"/login.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
