package org.chris.utils;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public class ReqUtils {
	public static Map<String, Object> getParams(HttpServletRequest request){
		Map<String, Object> condition = new HashMap<String, Object>();
		if(null!=request){
			Enumeration<?> paramNames = request.getParameterNames();
			if(paramNames!=null){
				while(paramNames.hasMoreElements()){
					String paramName = (String) paramNames.nextElement();
					String paramValue=request.getParameter(paramName);
					condition.put(paramName, paramValue);
				}
			}
		}
		return condition;
	}
}
