package org.chris.utils;


import java.util.Random;

public abstract class RandomUtils {
	/**
	 * 用java.util.UUID获得的随机的long
	 */
	public static long randomLong(){
		return java.util.UUID.randomUUID().getMostSignificantBits();
	}
	
	public static long randomABSLong(){
		return Math.abs(randomLong());
	}
	
	public static String randomByYMDHMS(int n){
		String rs = DateUtils.formatNow(DateUtils.DEFALUT_DATETIME_PATTERN_FORMAT_TWO);
		if(n>0){
			rs+="-"+randomNumByLength(n);
		}
		return rs;
	}
	
	/**
	 * 根据位数随机生成int格式
	 * @param 生成数字格式字符串的位数
	 */
	public static String randomString4Int(int digit){
		Random random = new Random();
		StringBuilder re = new StringBuilder(digit);
		
		for(int i=0;i<digit;i++){
			//i 0-9
			re.append(random.nextInt(10));
		}
		
		return re.toString();
	}
	
	public static String randomString4Letter(int digit){
		Random random = new Random();
		StringBuilder re = new StringBuilder(digit);
		
		boolean isBig;
		for(int i=0;i<digit;i++){
			//A-Z 65-90
			//a-z 97-122
			isBig = (random.nextInt(2)==1);
			if(isBig){
				re.append((char)(random.nextInt(26)+65));
			}else{
				re.append((char)(random.nextInt(26)+97));
			}
		}
		return re.toString();
	}
	/**随机生成N位数**/
	public static String randomNumByLength(int length){
		StringBuffer sb=new StringBuffer();
		Random random=new Random();
		for(int i=0; i<length; i++){
			sb.append(random.nextInt(10));
		}
		return sb.toString();
	}
	public static String getExt(String fileName){
		return StringUtils.hasText(fileName)?(fileName.substring(fileName.indexOf("."))):"";
	}
//	public static void main(String[] args) {
////		System.out.println(randomString4Int(5));
////		System.out.println(randomNumByLength(4));
//		int i=0; 
//		while(i<10){
////			System.out.println(java.util.UUID.randomUUID().getMostSignificantBits());
//		System.out.println(java.util.UUID.randomUUID().toString());
//			i++;
//		}
//		
//	}
}
