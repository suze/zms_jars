package org.chris.utils;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * 字符串工具
 * @author sntey
 */

public abstract class StringUtils {
	protected final static Logger log = Logger.getLogger(StringUtils.class);
	/**ISO-8859-1*/
	public static final String ISO_8859_1="ISO-8859-1";
	/**UTF-8*/
	public static final String UTF_8="UTF-8";

	public abstract class Symbol{
		/** [] */
		public static final String EMPTY = "";
		
		/** [.] 句点 */
		public static final String POINT = ".";
		
		/** [,] 逗号 */
		public static final String COMMA = ",";
		
		/** [/] */
		public static final String SLASH="/";
		
		/** [\] */
		public static final String BACKSLASH="\\";
		
		/** ['] */
		public static final String QM="\'";
		
		/** ["] */
		public static final String DQM="\"";
		
		/** [全角空格] */
		public static final String BLANK_SBC="　";
		
		/** [半角空格] */
		public static final String BLANK_DBC=" ";
	};
	
	

	/**
	 * 判断 参数 sb 是否为 null,""," "
	 * @param sb
	 * @return
	 */
	public static final boolean notText(StringBuffer sb){
		return !hasText(sb);
	}
	
	/**
	 * 是否不为null,"" 或只含有" "(空格)
	 * */
	public static final boolean hasText(StringBuffer sb){
		return (sb != null) && (hasText(sb.toString()));
	}
	public static final boolean hasText(Object sb){
		return (sb != null) && (hasText(sb.toString()));
	}
	
	/**
	 * 是否为null,"" 或只含有" "(空格)
	 * */
	public static final boolean notText(String str){
		return !hasText(str);
	}
	
	/**
	 * <br>字符串是否有非空内容 
	 * <br>StringUtils.hasText(null) -- false
	 * <br>StringUtils.hasText("") -- false
	 * <br>StringUtils.hasText(" ") -- false
	 * <br>StringUtils.hasText("12345") -- true
	 * <br>StringUtils.hasText(" 12345 ") -- true
	 */
	public static final boolean hasText(String str){
		if(str == null || "null".equals(str)) return false;
		String use = str.replace("'", "");
		
		return (use != null && use.trim().length() > 0);
	}
	
	/**
	 * <br>字符串是否有长度 
	 * <br>StringUtils.hasText(null) -- false
	 * <br>StringUtils.hasText("") -- false
	 * <br>StringUtils.hasText(" ") -- true
	 * <br>StringUtils.hasText("12345") -- true
	 * <br>StringUtils.hasText(" 12345 ") -- true
	 */
	public static boolean hasLength(String str) {
		if(str == null) return false;
		return (str != null && str.length() > 0);
	}
	
	
	
	public static boolean hasLength(String[] strs) {
		if(strs == null) return false;
		
		return strs.length>0;
	}
	
	public static String[] trimAll(String[] array){
		if(array==null){	return array;}
		String[] result = new String[array.length];
		for(int i=0;i<array.length;i++){
			result[i]=array[i]==null?null:array[i].trim();
		}
		return result;
	}
	
	/**
	 * 数组转化成字符串,
	 * 使用默认的连接符号
	 * */
	public static <T> String arrayToString(T[] array) {
		return arrayToString(array, ",", "", "");
	}
	
	/**
	 * 数组转化成字符串,
	 * 使用自定义的连接符号
	 * */
	public static <T> String arrayToString(T[] array,String connectSymbol) {
		return arrayToString(array, connectSymbol, "", "");
	}
	
	/**
	 * 数组转化成字符串,
	 * 使用逗号连接[,]
	 * 并加上自定义的首 尾标志
	 * @param leading 首标志
	 * @param trailing 尾标志
	 * */
	public static <T> String arrayToString(T[] array,String leading,String trailing){
		return arrayToString(array, ",", leading, trailing);
	}
	
	/**
	 * 数组转化成字符串,
	 * 使用自定义的连接符号
	 * 并加上自定义的首 尾标志
	 * @param connectSymbol 连接符号
	 * @param leading 首标志
	 * @param trailing 尾标志
	 * */
	public static <T> String arrayToString(T[] array,String connectSymbol,String leading,String trailing) {
		connectSymbol = (connectSymbol == null?"":connectSymbol);
		leading = (leading == null?"":leading);
		trailing = (trailing == null?"":trailing);
		int len = array.length;
		if ( len == 0 ) return "";
		StringBuffer buf = new StringBuffer( len * 12 + leading.length() + trailing.length());
		for ( int i = 0; i < len - 1; i++ ) {
			buf.append(leading).append( array[i].toString() ).append(trailing)
					.append(connectSymbol);
		}
		return buf.append(leading).append( array[len - 1].toString() ).append(trailing).toString();
	}

	
	/**
	 * 字符串编码转换 使用默认的编码 {@link #DEFAULT_CODING}
	 * @see #convertStrByCoding(String, String)
	 * 
	 * @param str 需要转换的字符串
	 * @return 
	 */
	public static String convertStrByCoding(String str){
		return convertStrByCoding(str,ISO_8859_1);
	}
	
	/**
	 * 字符串编码转换 使用自定义编码
	 * @param str    需要转换的字符串
	 * @param coding 编码
	 * @return
	 */
	public static String convertStrByCoding(String str, String coding) {
		try {
			return new String(str.getBytes(coding));
		} catch (Exception e) {
			log.error("string coding is error!", e);
			return null;
		}
	}
	/**
	 * 字符串字串替换
	 */
	public static String replace(String inString, String oldPattern, String newPattern) {
		if (inString == null) {
			return null;
		}
		if (oldPattern == null || newPattern == null) {
			return inString;
		}

		StringBuffer sbuf = new StringBuffer();
		// output StringBuffer we'll build up
		int pos = 0; // our position in the old string
		int index = inString.indexOf(oldPattern);
		// the index of an occurrence we've found, or -1
		int patLen = oldPattern.length();
		while (index >= 0) {
			sbuf.append(inString.substring(pos, index));
			sbuf.append(newPattern);
			pos = index + patLen;
			index = inString.indexOf(oldPattern, pos);
		}
		sbuf.append(inString.substring(pos));

		// remember to append any characters to the right of a match
		return sbuf.toString();
	}

	public static String collectionToString(Collection<?> coll, String connectSymbol) {
		return collectionToString(coll,connectSymbol,"","");
	}
	
	public static String collectionToString(Collection<?> coll, String connectSymbol,String leading,String trailing) {
		return arrayToString(coll.toArray(), connectSymbol, leading, trailing);
	}
	
	public static List<String> collectionToStringList(Collection<?> coll){
		if(coll == null || coll.size() == 0){	return new ArrayList<String>(0);}
		List<String> result = new ArrayList<String>(coll.size()>10000?10000:coll.size());
		
		for(Object o:coll){
			if(o instanceof String
					|| o instanceof Number){
				result.add(o.toString());
			}else if(o instanceof Date){
				result.add(DateUtils.format((Date)o));
			}
		}
		return result;
	}

	/**
	 * 在数组的指定位置插入指定个数的元素
	 * */
	public static String[] addToArray(String[] array, String...strs) {
		return addToArray(array, 0, strs);
	}
	
	/**
	 * 在数组的指定位置插入指定个数的元素,
	 * <br>将当前处于该位置的元素（如果有的话）和所有后续元素向右移动（在其索引中加指定个数).
	 * @param array 源数组
	 * @param index 插入位置
	 * @param strs 插入内容 支持变参
	 * @return 新数组 
	 * */
	public static String[] addToArray(String[] array, int index, String...strs) {
		if(strs == null || strs.length == 0){	return array;}
		
		if(index<0 || index>array.length){	
			throw new IndexOutOfBoundsException(
				"索引:["+index+"]小于0 或者 大于数组长度: ["+array.length+"]"); 
		}
		
		String[] result = new String[array.length + strs.length];
		
		System.arraycopy(array, 	0, result,					 0,                index);
		System.arraycopy(strs, 		0, result, 				 index, 		 strs.length);
		System.arraycopy(array, index, result, index + strs.length, array.length - index);
		
		return result;
	}
	
	/**
	 * 按次数输出
	 * */
	public static String outByTimes(String s, int times) {
		if(s==null){	return s;}
		
		StringBuilder builder = new StringBuilder(s.length()*times>2000?2000:s.length()*times);
		for(int i=0;i<times;i++){
			builder.append(s);
		}
		return builder.toString();
	}
	
	public static String removeChars(String s,char...chars){
		if(s==null || chars==null){	return s;}
		String temp = new String(s);
		for(char c:chars){
			temp = StringUtils.replace(temp, String.valueOf(c), "");
		}
		return temp;
	}
	
	/**
	 * 将 s 转换成 UTF-8 格式并返回.
	 * @param s
	 * @return
	 */
	public static String toUtf8(String s) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 0 && c <= 255) {
				sb.append(c);
			} else {
				byte[] b;
				try {
					b = Character.toString(c).getBytes("utf-8");
				} catch (Exception ex) {
					b = new byte[0];
				}
				for (int j = 0; j < b.length; j++) {
					int k = b[j];
					if (k < 0)
						k += 256;
					sb.append("%" + Integer.toHexString(k).toUpperCase());
				}
			}
		}

		return sb.toString();
	}
	
	/**
	 * 获得拼音简码
	 */
	public static String getPYSimpleName(String a){
		if(!StringUtils.hasText(a)){		return "";}
		char[] chars=a.toCharArray();
		StringBuffer strBuffer=new StringBuffer(chars.length);
		for(int i=0;i<chars.length;i++)
		{
			strBuffer.append(getPYHeadLetter(chars[i]));
		}
		return strBuffer.toString();
	}
	
	/**
	 * 获得汉字拼音的首字母,如果参数不符号要求
	 * 则返回自身(单字符) or ' '(无法解析的双字符) 
	 * */
	public static char getPYHeadLetter(char aChar)
	{
		char result;
//		boolean flag = true;
		int charAscii=getCharAscii(aChar);
		if	   (0<=charAscii && charAscii<=255)		  result=aChar;		//Ascii 小于 256 返回自身
		else if(charAscii>=45217 && charAscii<=45252) result= 'A';
		else if(charAscii>=45253 && charAscii<=45760) result= 'B';
		else if(charAscii>=45761 && charAscii<=46317) result= 'C';
		else if(charAscii>=46318 && charAscii<=46825) result= 'D';
		else if(charAscii>=46826 && charAscii<=47009) result= 'E';
		else if(charAscii>=47010 && charAscii<=47296) result= 'F';
		else if(charAscii>=47297 && charAscii<=47613) result= 'G';
		else if(charAscii>=47614 && charAscii<=48118) result= 'H';
		else if(charAscii>=48119 && charAscii<=49061) result= 'J';
		else if(charAscii>=49062 && charAscii<=49323) result= 'K';
		else if(charAscii>=49324 && charAscii<=49895) result= 'L';
		else if(charAscii>=49896 && charAscii<=50370) result= 'M';
		else if(charAscii>=50371 && charAscii<=50613) result= 'N';
		else if(charAscii>=50614 && charAscii<=50621) result= 'O';
		else if(charAscii>=50622 && charAscii<=50905) result= 'P';
		else if(charAscii>=50906 && charAscii<=51386) result= 'Q';
		else if(charAscii>=51387 && charAscii<=51445) result= 'R';
		else if(charAscii>=51446 && charAscii<=52217) result= 'S';
		else if(charAscii>=52218 && charAscii<=52697) result= 'T';
		else if(charAscii>=52698 && charAscii<=52979) result= 'W';
		else if(charAscii>=52980 && charAscii<=53640) result= 'X';
		else if(charAscii>=53689 && charAscii<=54480) result= 'Y';
		else if(charAscii>=54481 && charAscii<=62289) result= 'Z';
		else {
			log.warn("无法解析字母["+aChar+"]Ascii["+charAscii+"]");
			result=' ';
//			flag = false;
		}
//		if(flag){
//			log.info("字母["+aChar+"]Ascii["+charAscii+"]");
//		}
		
		//返回小写
		return Character.toLowerCase(result);
	}
	
	
	public static String parse(Double d){

		return parse(d, 0);
	}
	public static String parse(Object o , int m){
		if(m<0) throw new RuntimeException("格式不对");
		
		
		String formatString = "0";
		if(m>0){
			formatString+=".";
		}
		for(int i=1; i<=m; i++){
			formatString +="0";
		}
		DecimalFormat df = new DecimalFormat(formatString);
		
		return df.format(o);
		
	}
	/**
	 * 获得单个字符的Ascii.
	 * 
	 * @param char
	 *            汉字字符
	 * @return int 错误返回 0,否则返回ascii
	 */
	public static int getCharAscii(char aChar) {
		byte[] bytes = (String.valueOf(aChar)).getBytes();
		if (bytes == null || bytes.length > 2 || bytes.length <= 0) {
			return 0;
		}
		
		if (bytes.length == 1) {
			return bytes[0];
		}
		else if (bytes.length == 2) {
			int hightByte = 256 + bytes[0];
			int lowByte = 256 + bytes[1];
			int ascii = (256 * hightByte + lowByte);
			return ascii;
		}else{
			log.warn("无法获得字符["+aChar+"]的Ascii编码");
			return 0;
		}
	}
	/**
	 * @see 得到一个新的字符串 如"AA、BB、CC、"，处理后返回"AA、BB、CC"
	 * @since 2011-11-23
	 * @author ShiDongdong
	 * @param str 处理字符串
	 * @return 处理后字符串
	 */
	public static String getNewStr(String str){
		return (!"、".equals(str)&&StringUtils.hasText(str))?str.substring(0, str.length()-1):str;
	}
	/**
	 * 格式输出新字符串
	 * @Title: formatSysmbol 
	 * @author Chris Suk
	 * @date 2012-10-18 下午02:18:54 
	 * @param value
	 * @param sysmbol
	 * @return
	 */
	public static String formatSysmbol(String value,String sysmbol){
		return (StringUtils.hasText(value)&&value.endsWith(sysmbol))?value.substring(0, value.length()-1):value;
	}
	/**
	 * 根据ID字符串转换成map集合对象
	 * @author Chris
	 * @date Mar 21, 2012
	 * @time 12:16:14 PM 
	 * @param ids
	 * @return
	 */
	public static Map getMap(String ids){
		Map map = new HashMap();
		if(hasText(ids)){
			String[]s = ids.split(",");
			if(null!=s&&s.length>0){
				for (int i = 0; i < s.length; i++) {
					String id = s[i];
					if(hasText(s))
						log.warn("转换的对象中包含空对象！");
					map.put(i+1, id);
				}
			}
		}
		return map;
	}
	/**
	 * 从List集合中读取对象名称 返回字符串
	 * @Title: getStringFromList 
	 * @author Chris Suk
	 * @date 2012-3-22 下午02:04:27 
	 * @param list
	 * @return
	 */
	public static String getStringFromList(List list){
		String l="";
		if (!CollectionUtils.isEmpty(list)) {
			for(int i=0;i<list.size();i++){
				String s = (String)list.get(i);
				if(!StringUtils.hasText(s)){
					log.error("ERROR:转换List时出现空对象>>>");
				}else{
					l+=s+"、";
				}
			}
		}
		return StringUtils.hasLength(l)?l.substring(0, l.length()-1):l;
	}
	/**
	 * 获取集合ID字符串 返回1,3,4,
	 * @Title: getString 
	 * @author Chris Suk
	 * @date 2012-3-30 下午03:13:35 
	 * @param list
	 * @return
	 */
	public static String getString(List<Integer> list){
		String result="";
		if (!CollectionUtils.isEmpty(list)) {
			for(Integer value : list){
				if(value!=null&&value!=0){
					result+=value+",";
				}
			}
		}
//		return StringUtils.hasLength(result)?result.substring(0, result.length()-1):result;
		return result;
	}
	/**
	 * 判断输入当前值
	 * @Title: formatValue 
	 * @author Chris Suk
	 * @date 2012-4-12 上午10:25:44 
	 * @param value
	 * @return
	 */
	public static String formatValue(String value){
		return StringUtils.hasText(value)?value:"";
	}
	/**
	 * 判断输入当前值
	 * @Title: formatValue 
	 * @author Chris Suk
	 * @date 2012-4-12 上午10:25:44 
	 * @param value
	 * @return
	 */
	public static String formatValue(Object value){
		return null!=value?value.toString():"";
	}
	/**
	 * 判断输入当前值 如果该值为空 则返回symbol
	 * @Title: formatValue 
	 * @author Chris Suk
	 * @date 2012-4-12 上午10:26:01 
	 * @param value
	 * @param symbol
	 * @return
	 */
	public static String formatValue(Object value,String symbol){
		return null!=value?value.toString():symbol;
	}
	
	/**
	 * 判断输入当前值 如果该值为空 则返回symbol
	 * @Title: formatValue 
	 * @author Chris Suk
	 * @date 2012-4-12 上午10:26:01 
	 * @param value
	 * @param symbol
	 * @return
	 */
	public static String formatValue(String value,String symbol){
		return StringUtils.hasText(value)?value:symbol;
	}
	
	/**
	 * 处理字符串符合SQL格式
	 * @param s 字符串
	 */
	public static String formatSQL(String sql) {
		return formatValue(sql).replaceAll("'", "''");
	}
	/**
	 * 转换int类型
	 * @author Chris Suk
	 * @date 2014-4-26 下午02:29:43
	 * @param value
	 * @return
	 */
	public static int toInt(String value){
		return hasText(value)?Integer.parseInt(value):0;
	}
}
