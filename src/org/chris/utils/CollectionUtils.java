package org.chris.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public abstract class CollectionUtils {

	/** 集合元素替换 */
	public static void replace(List<Object> collection,Object oldObj,Object newObj){
		if(oldObj == null){
			oldObj = null;
		}
	
		synchronized (collection) {
			int index = 0;
			for (Object obj : collection) {
				if (oldObj.equals(obj)) {
					collection.set(index, newObj);
				}
				index++;
			}
		}		
	}
	
	/** 去除 toString 方法为空串的�? */
	public static <T> Collection<T> delectToStringEmptyValues(Collection<T> collection){
		List<T> list = new ArrayList<T>(safeInitSize(collection.size()));
		for(T o:collection){
			if(o == null){
				continue;
			}
			if(StringUtils.hasText(o.toString())){
				list.add(o);
			}
		}
		return list;
	}
	
	public static boolean isEmpty(Collection coll) {
		return (coll == null || coll.size()==0);
	}

	public static boolean isEmpty(Map map) {
		return (map == null || map.isEmpty());
	}


	public static <T> List<T> toList(Collection<T> coll) {
		List<T> list = new ArrayList<T>(safeInitSize(coll.size()));
		for(T o:coll){
			list.add(o);
		}
		return list;
	}

	private static int safeInitSize(int arg) {
		return arg < 2000 ?  arg : 2000;
	}

	public static int[] nice(String value){
		final String spliter = "->";
		String[] vs = value.split(spliter);
		int from = Integer.parseInt(vs[0]);
		int to = Integer.parseInt(vs[1]);
		int[] arrays = new int[to-from+1];
		if(null!=value){
			for(int i=0;from<=to;i++){
				arrays[i]=from;
				from++;
			}
		}
		return arrays;
	}
	
}
