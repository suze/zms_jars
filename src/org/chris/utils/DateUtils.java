package org.chris.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具
 * @author sntey
 */
public class DateUtils {
	
	public static final String YEAR="年";
	public static final String MONTH="月";
	public static final String DAY="日";
	public static final String SEASON="季";
	public static final String HELF_YEAR="半年";
	public static final String HELF_MONTH="半月";
	public static final String TEN_DAYS="旬";
	public static final String WEEK="周";
	public static SimpleDateFormat sf;
	
	private DateUtils(){}
	
	public static enum DateIntervalType {
		YEAR,
		MONTH,
		WEEK,
		DAY,
		HOUR,
		MINUTE,
		SECOND
	}
	
	/**1秒的毫秒值*/
	public static final int ONE_SECOND = 1000;
	/**1分钟的毫秒值*/
	public static final int ONE_MINUTE = 60000;
	/**1小时的毫秒值*/
	public static final int ONE_HOUR = 3600000;
	/**1天的毫秒值*/
	public static final long ONE_DAY = 86400000L;
	/**1年的毫秒值*/
	public static final long ONE_WEEK = 604800000L;
	
	/**  yyyy-MM-dd */
	public static final String DEFALUT_DATE_PATTERN="yyyy-MM-dd";
	/**  yyyy-MM-dd HH:mm:ss */
	public static final String DEFALUT_DATETIME_PATTERN="yyyy-MM-dd HH:mm:ss";
	/**  yyyy-MM-dd-HH-mm-ss */
	public static final String DEFALUT_DATETIME_PATTERN_FORMAT_TWO="yyyy-MM-dd-HH-mm-ss";
	/**  yyyy-MM-dd-HH-mm-ss */
	public static final String DEFALUT_DATETIME_PATTERN_FORMAT_NO_SPETOR="yyyyMMddHHmmss";
	
	public static final String PATTERN_CHINESE="yyyy年MM月dd日 E";
	
	/** 得到当前时间,返回值为java.util.Date类型 */
	public static Date getNow(){
		return new Date();
	}
	
	/**
	 * 按指定格式获得当前日期
	 * 返回一个字符串 yyyyMMddHHmmss 
	 * 		比如: 2007年10月15日 晚上9点30分20秒
	 * 		就返回  20071015213020
	 */
	public static String formatNow(String pattern){
		return format(getNow(),pattern);
	}
	/**
	 * 获得当前日期 格式是 yyyy-MM-dd HH:mm:ss
	 * @return String
	 */
	public static String formatNow(){
		return  formatNow(DEFALUT_DATETIME_PATTERN);
	}
	
	public static String formatNow2(){
		return  formatNow(DEFALUT_DATE_PATTERN);
	}
	
	/** 获得当前的年份 */
	public static int getNowYear() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.YEAR);
	}

	/** 获得当前的月份 */
	public static int getNowMonth() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.MONTH) + 1;
	}
	
	/** 获得当前的季份**/
	public static int getNowSeason(){
		return getNowMonth() / 3 + 1;
	}

	/** 获得当前的日 */
	public static int getNowDayOfMonth() {
		Calendar cal = Calendar.getInstance();
		return cal.get(Calendar.DAY_OF_MONTH);
	}
	
	/** 得到当前日期的 凌晨0点0分0秒 */
	public static Date getBeginOfToday(){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
	
	/** 得到当前日期的 凌晨23点59分59秒 */
	public static Date getEndOfToday(){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}
	
	/** 得到某天的开始时间 凌晨0点0分0秒 */
	public static Date getBeginOfDay(Date date){
		if(date==null)
			return null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date ds = new Date(cal.getTimeInMillis());
		return ds;
	}
	
	/** 得到某天的结束时间 凌晨23点59分59秒 */
	public static Date getEndOfDay(Date date) {
		if(date==null)
			return null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		Date ds = new Date(cal.getTimeInMillis());
		return ds;
	}
	
	/** 得到下一天 */
	public static Date getNextDay(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 1);
		return cal.getTime();
	}
	
	
	
	/** 得到昨天 */
	public static Date getYesterday() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(getBeginOfToday());
		cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) - 1);
		return cal.getTime();
	}
	
	/**
	 * 得到以当前日期为标准的X天
	 * X 为正，则是当前日期向后某天
	 * X 为负，则是当前日期向前某天
	 * **/
	public static Date getXDayByNow(int x){
		Calendar cal = Calendar.getInstance();
		cal.setTime(getEndOfToday());
		cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + x);
		return cal.getTime();
	}
	
	public static String getXDayByNow2(int x){
		Calendar cal = Calendar.getInstance();
		cal.setTime(getEndOfToday());
		cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + x);
		return format(cal.getTime(),DEFALUT_DATE_PATTERN);
	}

	/** 得到明天 */
	public static Date getTomorrow() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(getBeginOfToday());
		cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH) + 1);
		return cal.getTime();
	}
	/**
	 * 返回 date 里的年
	 * @param date
	 * @return
	 */
	public static int getYearOfDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.YEAR);
	}
	/**
	 * 返回 date 里的月
	 * @param date
	 * @return
	 */
	public static int getMonthOfDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.MONTH);
	}
	 /**
     * 获得指定日期在一年中的周数
     */
    public static int getWeeksOfYear(String date){
    	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try{
        	d = format.parse(date);
         }catch(Exception e){
        	e.printStackTrace();
         }
         Calendar cal = Calendar.getInstance();
         cal.setTime(d);
         int weeks = cal.get(Calendar.WEEK_OF_YEAR);
         return weeks;
    }
    /**
     * 获得指定日期在一年中的周数
     */
    public static int getWeeksOfYear(Date date){
         Calendar cal = Calendar.getInstance();
         cal.setTime(date);
         int weeks = cal.get(Calendar.WEEK_OF_YEAR);
         return weeks;
    }
	
	/** 得到当前月的第一天 */
	public static Date getFirstDayOfCurMonth() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, cal
				.getActualMinimum(Calendar.DAY_OF_MONTH));
		return getBeginOfDay(cal.getTime());
	}

	/** 得到当前月的最后一天 */
	public static Date getEndDayOfCurMonth(){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, cal
				.getActualMaximum(Calendar.DAY_OF_MONTH));
		return getBeginOfDay(cal.getTime());
	}
	
	/** 得到两个日期中较大者 */
	public static Date getMax(Date date1, Date date2){
		if (date1 == null)
			return date2;
		if (date2 == null)
			return date1;
		if (date1.after(date2))
			return date1;
		return date2;
	}

	/** 得到两个日期中较小者  */
	public static Date getMin(Date date1, Date date2){
		if (date1 == null)
			return date2;
		if (date2 == null)
			return date1;
		if (date1.after(date2))
			return date2;
		return date1;
	}
	
	/** 得到year年month月的天数 */
	public static int daysOfMonth(int year, int month){
		switch (month) {
		case 1:	case 3:	case 5:	case 7:
		case 8:	case 10:case 12:
			return 31;
		case 4:	case 6:	case 9:	case 11:
			return 30;
		case 2:
			if (isLeap(year))
				return 29;
			return 28;
		default:
			return 0;
		}
	}
	
	/**  判断一年是否是闰年 */
	public static boolean isLeap(int year) {
		boolean divBy4 = (year % 4 == 0);
		boolean divBy100 = (year % 100 == 0);
		boolean divBy400 = (year % 400 == 0);
		return divBy4 && (!divBy100 || divBy400);
	}
	
	/** 判断所传的 date 是否能转换成 Date */ 
	public static boolean isDate(String date) {
		try {
			parse(date);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/** 在指定日期基础上加若干年 */
	public static Date addYear(Date date, int i) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);
		calendar.set(Calendar.YEAR, i + year);
		return calendar.getTime();
	}
	
	/** 在指定日期基础上加若干月 */
	public static Date addMonth(Date date, int i) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int month = calendar.get(Calendar.MONTH);
		month += i;
		int deltaY = month / 12;
		month %= 12;
		calendar.set(Calendar.MONTH, month);
		if (deltaY != 0) {
			int year = calendar.get(Calendar.YEAR);
			calendar.set(Calendar.YEAR, deltaY + year);
		}
		return calendar.getTime();
	}
	
	/** 在指定日期基础上加若干天 */
	public static Date addDay(Date date, int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(5, day);
		Date d = calendar.getTime();
		return d;
	}

	/** 在指定日期基础上加若干小时 */
	public static Date addHour(Date date, long l) {
		long oldD = date.getTime();
		long deltaD = l * 60L * 60L * 1000L;
		long newD = oldD + deltaD;
		Date d = new Date(newD);
		return d;
	}

	/** 在指定日期基础上加若干分钟 */
	public static Date addMinute(Date date, long l) {
		long oldD = date.getTime();
		long deltaD = l * 60L * 1000L;
		long newD = oldD + deltaD;
		Date d = new Date(newD);
		return d;
	}
	
	/** 在指定日期基础上加若干秒 */
	public static Date addSecond(Date date, long l) {
		long oldD = date.getTime();
		long deltaD = l * 1000L;
		long newD = oldD + deltaD;
		Date d = new Date(newD);
		return d;
	}
	
	/** 在指定日期基础上加若毫秒 */
	public static Date addMilliSecond(Date date, long l) {
		long oldD = date.getTime();
		long deltaD = l;
		long newD = oldD + deltaD;
		Date d = new Date(newD);
		return d;
	}
	
	/**
	 * 在指定日期基础上加上当前的 时分秒
	 * @param date
	 */
	public static Date addNowHourMinSec(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		Calendar now = Calendar.getInstance();
		
		calendar.set(Calendar.HOUR_OF_DAY, now.get(Calendar.HOUR_OF_DAY));
		calendar.set(Calendar.MINUTE, now.get(Calendar.MINUTE));
		calendar.set(Calendar.SECOND, now.get(Calendar.SECOND));
		
		return new Date(calendar.getTimeInMillis());
	}
	
	
	/**
	 * 将 java.util.Date 转换成 String 使用默认的格式 (yyyy-MM-dd HH:mm:ss)
	 * @param aDate 需要转换的日期
	 * @return
	 * @see DEFALUT_DATETIME_PATTERN
	 */
	public static String format(Date aDate){
		return format(aDate,DEFALUT_DATETIME_PATTERN);
	}
	
	/**
	 * 将 java.util.Date 转换成 String 使用自定义的格式
	 * @param aDate 需要转换的日期
	 * @param pattern 样式
	 * @return
	 */
	public static String format(String aDate,String pattern){
		 SimpleDateFormat df = new SimpleDateFormat(pattern);
		 return df.format(parse(aDate));
	}
	/**
	 * 将 java.util.Date 转换成 String 使用自定义的格式
	 * @param aDate 需要转换的日期
	 * @param pattern 样式
	 * @return
	 */
	public static String format(Date aDate,String pattern){
		 SimpleDateFormat df = new SimpleDateFormat(pattern);
		 return df.format(aDate);
	}
	
	/**
	 * 将 String 转换成 java.util.Date 使用默认的格式 (yyyy-MM-dd)
	 * @param strDate 需要转换的日期
	 * @return
	 * @throws ParseException 
	 * @see DEFALUT_DATE_PATTERN
	 */
	public static Date parse(String strDate){
		return parse(strDate, new String[]{DEFALUT_DATE_PATTERN, DEFALUT_DATETIME_PATTERN});
	}
	
	/**
	 * 将 String 转换成 java.util.Date 使用自定义的格式 
	 * @param strDate 需要转换的日期
	 * @aMask 自定义的格式
	 * @return Date 
	 * @throws ParseException 
	 * @see DEFALUT_DATETIME_PATTERN
	 */
	public static Date parse(String strDate, String aMask){
		SimpleDateFormat df =new SimpleDateFormat(aMask);
		Date date = null;
		try {
			date = df.parse(strDate);
		} catch (Exception e) {
			return null;
		}
		return date;
	}

	public static Date parseAddBeginOfDay(String date){
		return parse(date+" 00:00:00",DateUtils.DEFALUT_DATETIME_PATTERN);
	}
	public static Date parseAddEndOfDay(String date){
		return parse(date+" 23:59:59",DateUtils.DEFALUT_DATETIME_PATTERN);
	}
	public static Date parse(String strDate, String[] aMasks){
		for(String aMask:aMasks){
			Date date = parse(strDate, aMask);
			if(date!=null){
				return date;
			}
		}
		return null;
	}
	public static String getCurrentZhDate(){
		StringBuffer sb=new StringBuffer();
		sb.append(getNowYear());
		sb.append(YEAR);
		sb.append(getNowMonth());
		sb.append(MONTH);
		sb.append(getNowDayOfMonth());
		sb.append(DAY);
		
		return sb.toString();
	}
	/**
	 * 得到两个日期之间的时间间隔,根据间隔参数
	 * 
	 * @param interval
	 *            间隔参数 计算数量参数( 年,月,日,时 ...). 取值
	 *            DateIntervalTypeEnum.YEAR,DateIntervalTypeEnum.MONTH, ...
	 * @param dDate1
	 * @param dDate2
	 * @return dDate1-dDate2 之间的时间间隔. 如果 dDate1 > dDate2,返回负数
	 */
	public static long dateDiff(DateIntervalType interval, Date dDate1,
			Date dDate2) {
		int desiredField = 0;
		int coef = 1;
		Date date1;
		Date date2;
		if (dDate1.getTime() > dDate2.getTime()) {
			coef = -1;
			date1 = dDate2;
			date2 = dDate1;
		} else {
			date1 = dDate1;
			date2 = dDate2;
		}
		int field;
		if (interval.ordinal() == DateIntervalType.YEAR.ordinal())
			field = Calendar.YEAR;
		else if (interval.ordinal() == (DateIntervalType.MONTH.ordinal()))
			field = Calendar.MONTH;
		else if (interval.ordinal() == (DateIntervalType.DAY.ordinal()))
			field = Calendar.DAY_OF_MONTH;
		else if (interval.ordinal() == (DateIntervalType.WEEK.ordinal()))
			field = Calendar.WEEK_OF_MONTH;
		else if (interval.ordinal() == (DateIntervalType.HOUR.ordinal())) {
			field = Calendar.DATE;
			desiredField = Calendar.HOUR_OF_DAY;
		} else if (interval.ordinal() == (DateIntervalType.MINUTE.ordinal())) {
			field = Calendar.DATE;
			desiredField = Calendar.MINUTE;
		} else if (interval.ordinal() == (DateIntervalType.SECOND.ordinal())) {
			field = Calendar.DATE;
			desiredField = Calendar.SECOND;
		} else {
			throw new IllegalArgumentException("unkown interval!");
		}
		Calendar calTmp = Calendar.getInstance();
		calTmp.setTime(date1);
		long nbOccurence = 0L;
		calTmp.add(field, 1);
		Date dateTemp = calTmp.getTime();
		while (dateTemp.getTime() <= date2.getTime()) {
			calTmp.add(field, 1);
			dateTemp = calTmp.getTime();
			nbOccurence++;
		}
		if (desiredField == Calendar.HOUR_OF_DAY
				|| desiredField == Calendar.MINUTE
				|| desiredField == Calendar.SECOND) {
			calTmp.setTime(date1);
			calTmp.add(field, (int) nbOccurence);
			dateTemp = calTmp.getTime();
			switch (desiredField) {
			case Calendar.HOUR_OF_DAY:
				nbOccurence *= 24L;
				break;
			case Calendar.MINUTE:
				nbOccurence = nbOccurence * 24L * 60L;
				break;
			case Calendar.SECOND:
				nbOccurence = nbOccurence * 24L * 60L * 60L;
				break;
			}
			calTmp.add(desiredField, 1);
			dateTemp = calTmp.getTime();
			while (dateTemp.getTime() <= date2.getTime()) {
				calTmp.add(desiredField, 1);
				dateTemp = calTmp.getTime();
				nbOccurence++;
			}
		}
		return nbOccurence * coef;
	}
	
	/**
	 * 将形如:'2011-8-3'形式的日期转换成形如:'2011-08-03'形式的日期
	 * @param stringDate,日期字符串
	 * @return 形如:'2011-08-03'形式的日期
	 * @author ZhouBo
	 */
	public static String getFormatStringDate(String stringDate){
		if(stringDate==null || "".equals(stringDate))
			return null;
		String[] dt = stringDate.split("-");
		StringBuffer buffer = new StringBuffer();
		for(int i=0;i<dt.length;i++){
			if(i==0)
				buffer.append(dt[i]);
			else{
				if(dt[i].length()<2)
					dt[i] = "0"+dt[i];
				if(i==1)
					buffer.append("-"+dt[i]);
				else if(i==2)
					buffer.append("-"+dt[i]);
			}
		}
		buffer.append(" 00:00:00");
		return buffer.toString();
	}
	
	/**
	 * 根据String型日期获取年、月、日
	 * @param stringDate
	 * @return String数组
	 * @author ZhouBo
	 * @since 2011-08-11
	 */
	public static String[] getObjectByStringDate(String stringDate){
		String[] o = new String[3];
		String[] str = stringDate.split("-");
		for(int i=0;i<str.length;i++){
			if(i==0)
				o[i] = str[i];
			else{
				if(str[i].indexOf("0")>=0)
					o[i] = str[i].replace("0", "");
				else
					o[i] = str[i];
			}
		}
		return o;
	}
	
	/**
	 * 获取某年某月的最后一天,注意：月份是从0开始的,如果传1表示2月份
	 * @param year,年份
	 * @param month,月份
	 * @return 月的最后一天
	 * @author ZhouBo
	 * @since 2011-08-14
	 */
	public static String getLastDayOfMonth(int year, int month) {      
        Calendar cal = Calendar.getInstance();      
        cal.set(Calendar.YEAR, year);      
        cal.set(Calendar.MONTH, month);      
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DATE));   
       return format(cal.getTime(),DEFALUT_DATE_PATTERN);   
    }
	
	/**
	 * 获取某年某月的第一天,注意：月份是从0开始的,如果传1表示2月份
	 * @param year,年份
	 * @param month,月份
	 * @return 月的第一天
	 * @author ZhouBo
	 * @since 2011-08-14
	 */
    public static String getFirstDayOfMonth(int year, int month) {      
        Calendar cal = Calendar.getInstance();      
        cal.set(Calendar.YEAR, year);      
        cal.set(Calendar.MONTH, month);   
        cal.set(Calendar.DAY_OF_MONTH,cal.getMinimum(Calendar.DATE));   
       return format(cal.getTime(),DEFALUT_DATE_PATTERN);   
    }    
    
    /**
     * 获取当前日期是星期几<br>
     * @param date
     * @return 当前日期是星期几
     */
    public static String getWeekOfDate(Date date) {
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }
    
    /**
     * 获取当前日期是星期几<br>
     * @param date
     * @return 当前日期是星期几
     */
    public static int getWeekOfDate2Int(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w <= 0)
            w = 7;
        return w;
    }
    
    
	
	
	
	//===============以下用于测试===============
	public static void main(String[] args) throws Exception {
		System.out.println(DateUtils.daysOfMonth(2012, 10));
//		testMain();
//		Calendar cal = Calendar.getInstance();
//        cal.setTime(DateUtils.parse("2012-10-08"));
//        System.out.println(getWeekOfDate2Int(DateUtils.parse("2012-10-08")));
//        cal.setTime(DateUtils.parse("2012-10-12"));
//        System.out.println(cal.get(Calendar.DAY_OF_WEEK));
//        cal.setTime(DateUtils.parse("2012-10-14"));
//        System.out.println(cal.get(Calendar.DAY_OF_WEEK));
//		System.out.println(getWeekOfDate2Int(DateUtils.parse("2012-10-14",DEFALUT_DATETIME_PATTERN_FORMAT_TWO)));
//		System.out.println(parse("2012-02-03", "yyyy年MM月dd日"));
//		SimpleDateFormat dateformat2=new SimpleDateFormat("yyyy年MM月dd日");   
//        String a2=dateformat2.format(parse("2012-02-03"));
//        System.out.println("时间2:"+a2); 
		
//		System.out.println(getXDayByNow(-7));
//		String s = getFormatStringDate("2011-8-3");
//		System.out.println(getFormatStringDate("2011-8-3"));
//		System.out.println("//////////////");
//		System.out.println(getObjectByStringDate("2011-08-03")[0]);
//		System.out.println(getObjectByStringDate("2011-08-03")[1]);
//		System.out.println(getObjectByStringDate("2011-08-03")[2]);
		
//		System.out.println(getFirstDayOfMonth(2011, 4));
//		System.out.println(getLastDayOfMonth(2011, 4));
	}

	private static void testMain() throws Exception{
		//System.out.println(getNow());
		//System.out.println(dateDiff(DateIntervalTypeEnum.HOUR, getNow(),getBeginOfToday()));
		//System.out.println(convertStringToDate("2007-10-9"));
//		System.out.println(format(getNow(),"yyyyMMddHHmmss"));
//		System.out.println(getNowMonth());
		
	}

	
	
	
}
