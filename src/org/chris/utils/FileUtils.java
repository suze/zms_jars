package org.chris.utils;



import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;


public class FileUtils {

	public static final String FILE_SYSTEM_NAME="TheFileNameIsSystemName";
	
	public static final String FILE_EXPAND_RAR=".rar";
	
	/**将文件转换为Properties 格式**/
	private static Properties readPropertiesFromFile(File file) {
		Properties p = null;
		InputStream is = null;
		try {
			p = new Properties();
			is = new FileInputStream(file);
			p.load(is);
		} catch (IOException e) {
		}finally{
			IoUtils.close(is);
		}
		return p;
	}
	
	private static String[] getKeysFromFile(File file, String... keys){
		Properties pr=readPropertiesFromFile(file);
		List<String> list=new ArrayList<String>();
		for(String key : keys){
			list.add(pr.getProperty(key));
		}
		return list.toArray(new String[0]);
	}
	/**公共读取 key=value 样式文件的方法**/
	public static String[] getKeysFromPath(String path, String... keys){
		return getKeysFromFile(getFile(path),keys);
	}
	
	public static File getFile(String filePath){
		File file=null;
		try{
			file=new File(filePath);
		}catch(Exception e){
			e.printStackTrace();
		}
		return file;
		
	}
	
	public static File getFile(String filePath,boolean creator) throws Exception{
		File file=null;
		try{
			file=new File(filePath);
			if(creator){
				if(!file.exists()){
					file.createNewFile();
				}
			}
		}catch(Exception e){
			throw new Exception("文件读取失败,文件不存在![001]");
		}
		return file;
		
	}
	/**
	 * 验证路径是否存在，如果不存在就创建路径
	 * **/
	public static void validatePath(String path){
		File file=getFile(path);
		if(!file.exists()){
			file.mkdirs();
		}
	}
	
	/**
	 * 跟据路径及文件名，创建文件，如果文件名为系统默认，用系统自动生成的文件名
	 * @throws Exception 
	 * **/
	public static File validatePath(String path , String fileName ,String expand) throws Exception{
		if(FILE_SYSTEM_NAME.equals(fileName)){
			fileName=getSystemFileName();
			
		}
		validatePath(path);
		
		String newFileName=path+"\\"+fileName;
		if(expand!=null){
			newFileName=newFileName+expand;
		}
		
		File toFile=getFile(newFileName,true);
		
		if(!toFile.exists() ){
			try {
				toFile.createNewFile();
			} catch (IOException e) {
				throw new Exception("创建文件错误!请检查路径!");
			}
		}
		
		return toFile;
	}

	/**
	 * 系统自动生成的文件名
	 * **/
	public static String getSystemFileName(){
		return RandomUtils.randomByYMDHMS(3);
	}
	
	
	
	public static File validateFile(String comFilePath, String toPath) throws Exception{
		return validateFile(comFilePath, toPath ,FILE_SYSTEM_NAME );
	}
	/**该方法为验证文件的通用方法*
	 * @throws Exception */
	public static File validateFile(String comFilePath, String toPath, String newFileName) throws Exception{
		
		File comFile=getFile(comFilePath);
		
		String comFileName=comFile.getName();
		File toFilePath=getFile(toPath);
		if(!toFilePath.exists() ){
			toFilePath.mkdirs();
		}
		
		String toFileAbsPath="";
		if(FILE_SYSTEM_NAME.equals(newFileName)){
			
			toFileAbsPath=toPath+"\\"+getSystemFileName();
		}else if(StringUtils.hasText(newFileName)){
			toFileAbsPath=toPath+"\\"+newFileName;
		}else {
			toFileAbsPath=toPath+"\\"+comFileName;
		}

		File toFile=getFile(toFileAbsPath);
		if(!toFile.exists() ){
			try {
				toFile.createNewFile();
			} catch (IOException e) {
				throw new Exception("文件传输出错!创建文件错误!请检查路径!");
			}
		}
		
		return toFile;
	}
	
//	/**
//	 * 验证文件，并返回将要保存的文件FILE
//	 * 如果文件名设为系统默认，则系统自动分配文件名、
//	 * 如果设的有文件名，则按自定义文件名保存。
//	 *否则保存为 来源文件的文件名
//	 * **/
//	public static File validateFile(FormFile comFile, String toPath, String newFileName){
//		Assert.isTrue(comFile!=null, "来源文件不存在!");
//		Assert.isTrue(StringUtils.hasText(comFile.getFileName()), "来源文件不存在!");
//		Assert.isTrue(StringUtils.hasText(toPath), "无保存路径");
//		String comFileName=comFile.getFileName();
//		File toFilePath=getFile(toPath);
//		if(!toFilePath.exists() ){
//			toFilePath.mkdirs();
//		}
//		
//		String toFileAbsPath="";
//		if(FILE_SYSTEM_NAME.equals(newFileName)){
//			
//			toFileAbsPath=toPath+"\\"+getSystemFileName();
//		}else if(StringUtils.hasText(newFileName)){
//			toFileAbsPath=toPath+"\\"+newFileName+comFileName.substring(comFileName.lastIndexOf("."), comFileName.length());
//		}else {
//			toFileAbsPath=toPath+"\\"+comFileName;
//		}
//
//		File toFile=getFile(toFileAbsPath);
//		if(!toFile.exists() ){
//			try {
//				toFile.createNewFile();
//			} catch (IOException e) {
//				throw new Exception("文件传输出错!创建文件错误!请检查路径!");
//			}
//		}
//		
//		return toFile;
//	}
	
	public static File copyFile(String comFilePath , String toPath ) throws Exception{
		return copyFile(comFilePath, toPath ,FILE_SYSTEM_NAME);
	}
	public static File copyFile(String comFilePath , String toPath ,String fileName) throws Exception{
		
		InputStream is=null;
		OutputStream os=null;
		File toFile = null;
		try{
			if(FILE_SYSTEM_NAME.equals(fileName)){
				fileName = getSystemFileName();
			}
			toFile=validateFile(comFilePath, toPath ,fileName);
			
			is=new FileInputStream(comFilePath);
			
			os=new FileOutputStream(toFile);
			
			byte[] b = new byte[is.available()];
			while(is.read(b)!=-1){
				os.write(b);
			}
		}catch(Exception e){
			throw new Exception("系统找不到指定文件，原因如下:  1,当前文件不在所运行的服务器上.");
		}finally{
			IoUtils.flush(os);
			IoUtils.close(os);
			IoUtils.close(is);
		}
		return toFile;
	}
	
	
	
	public static File validateFile(File file,String toPath, String newFileName) throws Exception{
		File toFilePath=getFile(toPath);
		if(!toFilePath.exists() ){
			toFilePath.mkdirs();
		}
		
		String toFileAbsPath="";
		if(FILE_SYSTEM_NAME.equals(newFileName)){
			
			toFileAbsPath=toPath+"/"+getSystemFileName();
		}else if(StringUtils.hasText(newFileName)){
			toFileAbsPath=toPath+"/"+newFileName;
		}else {
			toFileAbsPath=toPath+"/"+file.getName();
		}

		return getFile(toFileAbsPath+".xls",true);
		
	}
	public static File upLoad(File file, String toPath, String newFileName) throws Exception{
		InputStream is=null;
		OutputStream os=null;
		File toFile = null;
		try{
			toFile=validateFile(file,toPath ,newFileName);
			
			is=new FileInputStream(file);
			os=new FileOutputStream(toFile);
		
			
			byte[] b = new byte[is.available()];
			while(is.read(b)!=-1){
				os.write(b);
			}
		}catch (Exception e) {
			throw new Exception(e.getMessage());
		}finally{
			IoUtils.flush(os);
			IoUtils.close(os);
			IoUtils.close(is);
		}
		return toFile;
	}
	
//	/**
//	 * 来源路径
//	 * 保存路径
//	 * 保存文件名，如果为空，则默认保存文件名为来源路径的文件名
//	 * 返回：保存路径,返回的是相对路径
//	 * **/
//	public static File upLoad(FormFile comFile, String toPath, String newFileName){
//		
//		InputStream is=null;
//		OutputStream os=null;
//		File toFile = null;
//		try{
//			
//			
//			toFile=validateFile(comFile,toPath ,newFileName);
//			
//			is=comFile.getInputStream();
//			os=new FileOutputStream(toFile);
//		
//			
//			byte[] b = new byte[is.available()];
//			while(is.read(b)!=-1){
//				os.write(b);
//			}
//		}catch(Exception e){
//			throw new Exception(e.getMessage());
//		}finally{
//			IoUtils.flush(os);
//			IoUtils.close(os);
//			IoUtils.close(is);
//		}
//		return toFile;
//	}
	
	/**下载对应路径下所对应的文件
	 * 注：路径必须是完整路径，直接对应文件。
	 * @throws Exception 
	 * **/
	public static byte[] downloadFile(String downFilePath) throws Exception{
		
		try{
			return downloadFile(getFile(downFilePath));
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
		
	}
	public static byte[] downloadFile(File file) throws Exception{
		InputStream is = null;
		OutputStream os = null;
		try{
			is=new FileInputStream(file);
			os=new ByteArrayOutputStream();
			
			int len=0;
			byte[] b = new byte[is.available()];
			while((len = is.read(b))>0){
				os.write(b, 0, len);
				IoUtils.flush(os);
			}
			
			return b;
			
		
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}finally{
			IoUtils.close(os);
			IoUtils.close(is);
		}
		
	}
	
	
	/**根据路径删除对应的文件*
	 * @throws Exception */
	public static void removeFile(String path) throws Exception{
		try{
			File file=getFile(path);
			
			if (!file.isDirectory()) {
			    file.delete();
			}
			else if (file.isDirectory()) {
			    String[] filelist = file.list();
			    for (int i = 0; i < filelist.length; i++) {
			      File delfile = new File(path + "\\" + filelist[i]);
			  if (!delfile.isDirectory()) {
			    delfile.delete();
			  }
			  else if (delfile.isDirectory()) {
				  removeFile(path + "\\" + filelist[i]);
			      }
			    }
			    file.delete();
			}
		}catch(Exception e){
			throw new Exception("删除文件失败!");
		}
	}
	/**删除某一文件夹下所有文件*
	 * @throws Exception */
	public static void removeInnerFile(String path) throws Exception{
		try{
			File file=getFile(path);
			
			if (!file.isDirectory()) {
			}else if (file.isDirectory()) {
			    String[] filelist = file.list();
			    for (int i = 0; i < filelist.length; i++) {
			      File delfile = new File(path + "\\" + filelist[i]);
				  if (!delfile.isDirectory()) {
				    delfile.delete();
				  }else if (delfile.isDirectory()) {
					  removeFile(path + "\\" + filelist[i]);
			      }
			    }
			}
		}catch(Exception e){
			throw new Exception("删除文件失败!");
		}
	}
	
//	/**
//	 * 将多个文件打包
//	 * @throws Exception 
//	 * **/
//	public static File zipFiles(String[] srcs , String savePath) throws Exception{
//		File zipFile=validatePath(savePath, FILE_SYSTEM_NAME, FILE_EXPAND_RAR);
//		ZipOutputStream zipOs = null;
//		ByteArrayOutputStream bs = null;
//		try{
//			zipOs=new ZipOutputStream(zipFile);
//			
//			bs= new ByteArrayOutputStream();
//			for(String src : srcs){
//				FileInputStream in = new FileInputStream(src);
//				File file = getFile(src);
//				ZipEntry ze = new ZipEntry(file.getName());
//				zipOs.putNextEntry(ze);
//				int bt=in.read();
//				while(bt!=-1){
//					bs.write(bt);
//					zipOs.write(bt);
//					bt=in.read();
//				}
//				zipOs.closeEntry();
//				
//				IoUtils.close(in);
//			}
//			return zipFile;	
//		}catch(Exception e){
//			throw new Exception("打包下载出错!");
//		}finally{
//			IoUtils.close(zipOs);
//			IoUtils.close(bs);
//		}
//			
//	}
	
//	/**
//	 * 将多个文件打包
//	 * @throws Exception 
//	 * **/
//	public static File zipFiles(File[] files , String savePath) throws Exception {
//		File zipFile=validatePath(savePath, FILE_SYSTEM_NAME, FILE_EXPAND_RAR);
//		ZipOutputStream zipOs = null;
//		ByteArrayOutputStream bs = null;
//		try{
//			zipOs=new ZipOutputStream(zipFile);
//			
//			bs = new ByteArrayOutputStream();
//			
//			for(File file : files){
//				FileInputStream in = new FileInputStream(file);
//				ZipEntry ze = new ZipEntry(file.getName());
//				zipOs.putNextEntry(ze);
//				int bt=in.read();
//				while(bt!=-1){
//					bs.write(bt);
//					zipOs.write(bt);
//					bt=in.read();
//				}
//				zipOs.closeEntry();
//				
//				IoUtils.close(in);
//			}
//			return zipFile;		
//		}catch(Exception e){
//			throw new Exception("打包下载出错!");
//		}finally{
//			IoUtils.close(zipOs);
//			IoUtils.close(bs);
//		}
//		
//	}
	
	
	
	public static File createImage(String path,String name, String expand,byte[] b) throws Exception{
		File file = FileUtils.validatePath(path,name,expand);
		ImageOutputStream ios = null;
		
		try{
			ios = new FileImageOutputStream(file);
			ios.write(b);
		}catch (Exception e) {
			
		}finally{
			if(ios != null){
			ios.flush();
			ios.close();
			}
		}
		return file;
	}
	public static String getExt(String fileName){
		return StringUtils.hasText(fileName)?(fileName.substring(fileName.indexOf("."))):"";
	}
	public static void main(String[] s){
//		
//		String classPath=Class.class.getResource("/config/ExcelModelConfig.txt").getPath();
//		
//		String path=classPath;
//		String[] sw=FileUtils.getKeysFromPath(path, "notice");
//		
//		for(String ss : sw){
//			System.out.println(ss);
//		}

//		
//		
//		String comePath="F:\\Sntey\\demo\\comFile.txt";
//		String toPath="F:\\Sntey\\demo\\toFile";
//		try{
//			upLoad(comePath, toPath);
//		}catch(Exception e){
//			e.printStackTrace();
//		}
		
//		String path = "G:/";
//		String fn = "bxyb-m-2011-2-2011-08-14-220140.xls";
//		String p = path+"\\"+fn;
//		File f = getFile(p);
//		try {
//			f.createNewFile();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
}
