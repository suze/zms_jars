package org.chris.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.SupplierModel;

public interface SupplierService {
	/**
	 * 添加/修改
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param model
	 */
	public Map<String, Object> save(SupplierModel model);
	/**
	 * 删除
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param ids
	 */
	public Map<String, Object> delete(String...ids);
	/**
	 * 查询
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param id
	 */
	public SupplierModel find(int id);
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-5-25 下午07:22:04
	 * @return
	 */
	public List<SupplierModel> init();
	/**
	 * 分页查询
	 * @author Chris Suk
	 * @date 2014-5-12 下午09:57:01
	 * @param request
	 * @param rows
	 * @param page
	 * @return
	 */
	public Map<String, Object> query(HttpServletRequest request,int rows,int page);
}
