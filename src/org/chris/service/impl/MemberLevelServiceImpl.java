package org.chris.service.impl;

import java.util.List;

import org.chris.model.MemberLevel;
import org.chris.service.MemberLevelService;
import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.service.NameEntityService;
@IocBean(name="memberLevelService", fields={"dao"})
public class MemberLevelServiceImpl extends NameEntityService<MemberLevel> implements MemberLevelService {

	public void delete(String... ids) {
		this.dao().clear(MemberLevel.class, Cnd.where("id", "in", ids));
	}

	public List<MemberLevel> init() {
		return this.dao().query(MemberLevel.class, Cnd.orderBy().asc("orderNum"));
	}

	public void save(MemberLevel model) {
		MemberLevel query = this.dao().fetch(MemberLevel.class, model.getId());
		if(null!=query){
			this.dao().update(model);
		}else{
			this.dao().insert(model);
		}
	}
}
