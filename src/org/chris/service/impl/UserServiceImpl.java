package org.chris.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.base.ExtDaos;
import org.chris.model.UserModel;
import org.chris.service.UserService;
import org.nutz.dao.Cnd;
import org.nutz.dao.FieldFilter;
import org.nutz.dao.pager.Pager;
import org.nutz.dao.sql.Criteria;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Strings;
import org.nutz.service.IdEntityService;

@IocBean(name="userService", fields={"dao"})
public class UserServiceImpl extends IdEntityService<UserModel> implements UserService {

	public UserModel check(String username, String password) {
		Criteria cn = Cnd.cri();
		if(!Strings.isEmpty(username)){
			cn.where().andEquals("username", username);
		}
		if(!Strings.isEmpty(password)){
			cn.where().andEquals("password", password);
		}
		UserModel user=this.dao().fetch(UserModel.class, cn);
		return user;
	}

	public Map<String, Object> delete(String... ids) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			this.dao().clear(UserModel.class, Cnd.where("id", "in", ids));
			result.put("type", "0");
			result.put("message", "删除成功");
		} catch (RuntimeException e) {
			result.put("type", "1");
			result.put("message", e.getMessage());
			e.printStackTrace();
		}
		return result;
	}
	
	public UserModel find(int id){
		UserModel model = this.dao().fetch(UserModel.class, id);
		this.dao().fetchLinks(model, "employee");
		return model;
	}

	public Map<String, Object> query(HttpServletRequest request, int rows,int page) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			String username = request.getParameter("username");
			Criteria cn = Cnd.cri();
			if(!Strings.isEmpty(username)){
				cn.where().andEquals("username", username);
			}
			Pager pager = new Pager();
			pager.setPageNumber(page);
			pager.setPageSize(rows);
			List<UserModel> list = this.dao().query(UserModel.class, cn,pager);
			for(UserModel model : list){
				this.dao().fetchLinks(model, "employee");
			}
			result.put("total", list.size());
			result.put("rows", list);
		} catch (RuntimeException e) {
			e.printStackTrace();
		}
		return result;
	}

	public Map<String, Object> save(UserModel model) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			UserModel query = this.dao().fetch(UserModel.class, model.getId());
			if(null!=query){
//				ExtDaos.ext(dao(), FieldFilter.locked(UserModel.class, "^(createDate)$")).update(model);
				this.dao().update(model);
			}else{
				this.dao().insert(model);
			}
			result.put("type", "0");
			result.put("message", "保存成功");
		} catch (RuntimeException e) {
			result.put("type", "1");
			result.put("message", e.getMessage());
			e.printStackTrace();
		}
		return result;
	}

}
