package org.chris.service.impl;

import java.util.List;

import org.chris.model.ProductClass;
import org.chris.service.ProductClassService;
import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.service.NameEntityService;
@IocBean(name="productClassService", fields={"dao"})
public class ProductClassServiceImpl extends NameEntityService<ProductClass> implements ProductClassService {

	public void delete(String... ids) {
		this.dao().clear(ProductClass.class, Cnd.where("id", "in", ids));
	}

	public List<ProductClass> init() {
		return this.dao().query(ProductClass.class, Cnd.orderBy().asc("orderNum"));
	}

	public void save(ProductClass model) {
		ProductClass query = this.dao().fetch(ProductClass.class, model.getId());
		if(null!=query){
			this.dao().update(model);
		}else{
			this.dao().insert(model);
		}
	}
}
