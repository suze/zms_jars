package org.chris.service.impl;

import java.util.List;

import org.chris.model.DutyModel;
import org.chris.service.DutyService;
import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.service.NameEntityService;

@IocBean(name="dutyService", fields={"dao"})
public class DutyServiceImpl extends NameEntityService<DutyModel> implements DutyService {
	
	public void delete(String... ids) {
		this.dao().clear(DutyModel.class, Cnd.where("id", "in", ids));
	}

	public List<DutyModel> init() {
		return this.dao().query(DutyModel.class, Cnd.orderBy().asc("orderNum"));
	}

	public void save(DutyModel model) {
		DutyModel query = this.dao().fetch(DutyModel.class, model.getId());
		if(null!=query){
			this.dao().update(model);
		}else{
			this.dao().insert(model);
		}
	}

}
