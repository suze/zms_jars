package org.chris.service.impl;

import java.util.List;

import org.chris.model.BrandModel;
import org.chris.service.BrandService;
import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.service.NameEntityService;
@IocBean(name="brandService", fields={"dao"})
public class BrandServiceImpl extends NameEntityService<BrandModel> implements BrandService {

	public void save(BrandModel model) {
		BrandModel query = this.dao().fetch(BrandModel.class, model.getId());
		if(null!=query){
			this.dao().update(model);
		}else{
			this.dao().insert(model);
		}
	}

	public void delete(String... ids) {
		this.dao().clear(BrandModel.class, Cnd.where("id", "in", ids));
	}

	public List<BrandModel> init() {
		return this.dao().query(BrandModel.class, Cnd.orderBy().asc("orderNum"));
	}

}
