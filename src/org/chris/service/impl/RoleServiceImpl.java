package org.chris.service.impl;

import java.util.List;

import org.chris.model.RoleModel;
import org.chris.service.RoleService;
import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.service.IdEntityService;
@IocBean(name="roleService", fields={"dao"})
public class RoleServiceImpl extends IdEntityService<RoleModel> implements RoleService {

	public List<RoleModel> init() {
		return this.dao().query(RoleModel.class, Cnd.orderBy().asc("id"));
	}

	public void save(RoleModel model) {
		RoleModel query = this.dao().fetch(RoleModel.class, model.getId());
		if(null!=query){
			this.dao().update(model);
		}else{
			this.dao().insert(model);
		}
	}

}
