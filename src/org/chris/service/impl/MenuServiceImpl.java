package org.chris.service.impl;

import java.util.List;

import org.chris.model.MenuModel;
import org.chris.service.MenuService;
import org.nutz.dao.Cnd;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.service.IdEntityService;
@IocBean(name="menuService", fields={"dao"})
public class MenuServiceImpl extends IdEntityService<MenuModel> implements MenuService {

	public List<MenuModel> init() {
		return this.dao().query(MenuModel.class, Cnd.orderBy().asc("orderNum"));
	}

	public void save(MenuModel model) {
		MenuModel query = this.dao().fetch(MenuModel.class, model.getId());
		if(null!=query){
			this.dao().update(model);
		}else{
			this.dao().insert(model);
		}
	}


}
