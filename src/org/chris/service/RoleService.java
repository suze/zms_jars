package org.chris.service;

import java.util.List;

import org.chris.model.RoleModel;

public interface RoleService {
	/**
	 * 添加
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param model
	 */
	public void save(RoleModel model);
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:43:10
	 * @return
	 */
	public List<RoleModel> init();
}
