package org.chris.service;

import java.util.List;

import org.chris.model.MenuModel;

public interface MenuService {
	/**
	 * 添加
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param model
	 */
	public void save(MenuModel model);
	/**
	 * 加载菜单
	 * @author Chris Suk
	 * @date 2014-4-4 上午09:15:18
	 * @return
	 */
	public List<MenuModel> init();
}
