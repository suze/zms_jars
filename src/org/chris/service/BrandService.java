package org.chris.service;

import java.util.List;

import org.chris.model.BrandModel;

public interface BrandService {
	/**
	 * 添加
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param model
	 */
	public void save(BrandModel model);
	/**
	 * 删除
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param ids
	 */
	public void delete(String...ids);
	/**
	 * 加载
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:43:10
	 * @return
	 */
	public List<BrandModel> init();
}
