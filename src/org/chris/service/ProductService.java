package org.chris.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.ProductModel;

/**
 * 商品Service接口
 * @author Chris Suk
 * @date 2014-5-23 下午10:38:15
 */
public interface ProductService {
	/**
	 * 添加/修改
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param model
	 */
	public Map<String, Object> save(ProductModel model);
	/**
	 * 删除
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param ids
	 */
	public Map<String, Object> delete(String...ids);
	/**
	 * 分页查询
	 * @author Chris Suk
	 * @date 2014-5-12 下午09:57:01
	 * @param request
	 * @param rows
	 * @param page
	 * @return
	 */
	public Map<String, Object> query(HttpServletRequest request,int rows,int page);
}
