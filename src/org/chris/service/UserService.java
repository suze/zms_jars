package org.chris.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.chris.model.EmployeeModel;
import org.chris.model.UserModel;
/**
 * 用户service接口
 * @author Chris Suk
 * @date 2014-5-14 下午09:13:10
 */
public interface UserService {
	/**
	 * 登录验证
	 * @author Chris Suk
	 * @date 2014-5-14 下午09:13:03
	 * @param username
	 * @param password
	 * @return
	 */
	public UserModel check(String username,String password);
	/**
	 * 添加/修改
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param model
	 */
	public Map<String, Object> save(UserModel model);
	/**
	 * 删除
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param ids
	 */
	public Map<String, Object> delete(String...ids);
	/**
	 * 查询
	 * @author Chris Suk
	 * @date 2014-4-24 下午08:41:51
	 * @param id
	 */
	public UserModel find(int id);
	/**
	 * 分页查询
	 * @author Chris Suk
	 * @date 2014-5-12 下午09:57:01
	 * @param request
	 * @param rows
	 * @param page
	 * @return
	 */
	public Map<String, Object> query(HttpServletRequest request,int rows,int page);
}
