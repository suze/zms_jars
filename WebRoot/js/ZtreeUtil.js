var ZtreeUtil={
	edit:function(treeNode,url,params){
		var parentNode = treeNode.getParentNode();
		var pid=0;
		if(parentNode){
			pid=parentNode.id;
		}
		var data={
			id:treeNode.id,
			pid:pid,
			name:treeNode.name,
			params:params
		};
		$.post(url,data,function(){});
	},
	remove:function(treeNode,url,params){
		var data={
			id:treeNode.id,
			params:params
		};
		$.post(url,data,function(){});
	}
}