function openSelfDialog(url,title){
	dialog(title,url,"500","310");
}
/**
 * 个人项目设置弹出框
 * @param {} url
 * @param {} title
 */
function openSelfDialog2(url,title){
	dialog(title,url,"600","400");
}
/**
 * 打开短信任务设置弹出窗口
 */
function openSmsDialog(){
	var sUrl = "_sms_set.jsp?r=1";
	dialog('短信任务设置',sUrl,"500","310");
}
/**
 * 创建周或日计划
 * @param {} url
 * @param {} title
 * @param {} type
 */
function openTaskDialog(oMode,oDay,title,type){
	var sUrl = "calendar/m_calendar_append_view.jsp?mode="+oMode+"&day="+oDay+"&move=app&type="+type;
	dialog(title,sUrl,"540","380");
}