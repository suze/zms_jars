/**
 * 登录处理模块
 * @author Chris Suk
 * @date 2014/5/13
 * @type 
 */
var Login={
	/**
	 * 验证
	 */
	check:function(){
		var flag = false;
		$(':text,:password').each(function(){
			var _tip =$(this).attr('tip');
			var _value = $(this).val();
			if(!_value&&$(this).attr('required')){
				$(this).focus();
				$(this).poshytip({
					showOn: 'focus',
					alignTo: 'target',
					alignX: 'inner-left',
					offsetX: 0,
					offsetY: 5,
					content:_tip+'不能为空!'
				});
				return false;
				flag= false;
			}else{
				flag= true;
			}
		});
		if(flag){
			this.submit();
		}
	},
	/**
	 * 提交
	 */
	submit:function(){
		var params = $('#loginForm').serialize();
		$.getJSON('login.do',params,function(result){
			if(result.type=='0'){
				location.href='main.jsp';
			}else{
				$('#msg').html(result.msg);
			}
		});
	}
}