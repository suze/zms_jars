(function($){
	/**
	 * 封装easyUI的消息框
	 */
	$.msg=function(args){
		var opts = {
			title:'操作提示',
			msg:'',
			timeout:1000,
			showType:'slide',
			style:{
				right:'',
				top:document.body.scrollTop+document.documentElement.scrollTop,
				bottom:''
			}
		};
		if(typeof(args)=='string'){
			opts.msg=args;
			args=opts;
		}else{
			args=$.extend(opts,args);
		}
		$.messager.show(args);
	}
	/**
	 * @see 将form表单元素的值序列化成对象
	 * @requires jQuery
	 * @returns object
	 */
	$.serializeObject = function(form) {
		var o = {};
		$.each(form.serializeArray(), function(index) {
			if (o[this['name']]) {
				o[this['name']] = o[this['name']] + "," + this['value'];
			} else {
				o[this['name']] = this['value'];
			}
		});
		return o;
	};
})(jQuery);