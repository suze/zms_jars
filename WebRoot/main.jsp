<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="inc/include.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  	<title>欢迎登录ZMS(月光石)系统</title>
    <link rel="stylesheet" type="text/css" href="${thisPath}/plugins/jquery.easyui.1.3.6/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="${thisPath}/plugins/jquery.easyui.1.3.6/themes/icon.css">
	<script type="text/javascript" src="${thisPath}/plugins/jquery1.8.js"></script>
	<script type="text/javascript" src="${thisPath}/plugins/jquery.easyui.1.3.6/jquery.easyui.min.js"></script>
	<link rel="stylesheet" href="${thisPath}/plugins/jquery.ztree.3.5/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${thisPath}/plugins/jquery.ztree.3.5/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="${thisPath}/plugins/jquery.ztree.3.5/jquery.ztree.core-3.5.js"></script>
  </head>
  <body class="easyui-layout">
	<div id="top" data-options="region:'north',split:true,collapsed:false,border:false" style="height:60px;background: url('css/images/pick_bg.jpg')">
		<embed src="css/swf/logo.swf"  width="400" height="60"></embed>
		<div style="position: absolute; right: 0px;top: 0px;font-weight: bold;color: #777;font-size: 11px;">当前用户：<font color=red>龙影</font> 部门： 系统时间：2014年05月13日 星期二&nbsp;<span id='clock'></span></div>
	</div>
	<div id="left" data-options="region:'west',split:true,title:'系统菜单'" style="width:230px;padding:10px;"><ul id="menu" class="ztree"></ul></div>
	<div id="right" data-options="region:'east',split:true,collapsed:true,title:'系统消息'" style="width:100px;padding:10px;">east region</div>
	<div id="bottom" data-options="region:'south',split:true,collapsed:true,border:false" style="height:50px;background:#A9FACD;padding:10px;">版权所有&copy;Chris Suk</div>
	<div region='center' id="content">
		<div id="main" class="easyui-tabs" fit="true">
        	<div title="主页面" href="index.jsp" closable=true>
        		<button id="btn">测试</button>
        	</div> 
    	</div>
	</div>
	<!-- 右键菜单 -->
	<div id="menu" class="easyui-menu" style="width:150px;">
	    <div id="m-refresh">刷新</div>
	    <div class="menu-sep"></div>
	    <div id="m-closeall">全部关闭</div>
	    <div id="m-closeother">除此之外全部关闭</div>
	    <div class="menu-sep"></div>
	    <div id="m-close">关闭</div>
	</div>
  </body>
  <script type="text/javascript">
  	var ztree;
 	var setting = {
 		data: {
 			key:{
 				url:'_url'
 			},
			simpleData: {
				enable: true
			}
		},callback: {
			onClick:function(event, treeId, treeNode){
				var title = treeNode.title;
				var url=treeNode.url;
				if(!$("#main").tabs('exists',title)){// 如果不存在此tab则创建
					var content = '<iframe frameborder="0" name="'+title+'" id="'+treeId+'"src="'+url+'" scrolling="auto" style="width:100%;height:100%" ></iframe>';
			        if(url!="")
			            $("#main").tabs('add',{
			                title:treeNode.name, 
			                closable:true,
			                 //注：使用iframe即可防止同一个页面出现js和css冲突的问题  
				            content :  content
			            });
			    }else{// 如果已经打开则选中
			        $("#main").tabs('select',title);
			    }
			}
		}
	};
	$(function(){
	   $.getJSON('${thisPath}/login/menu/init.do?v='+new Date().getTime(),{id:'0'},function(data){
	    	data=eval(data);
	    	zTree=$.fn.zTree.init($("#menu"), setting, data);
	    });
	    //刷新
	    $("#m-refresh").click(function(){
	        var currTab = $('#tabs').tabs('getSelected');    //获取选中的标签项
	        var url = $(currTab.panel('options').content).attr('src');    //获取该选项卡中内容标签（iframe）的 src 属性
	        /* 重新设置该标签 */
	        $('#tabs').tabs('update',{
	            tab:currTab,
	            options:{
	                content: createTabContent(url)
	            }
	        })
	    });
	    
	    //关闭所有
	    $("#m-closeall").click(function(){
	        $(".tabs li").each(function(i, n){
	            var title = $(n).text();
	            $('#tabs').tabs('close',title);    
	        });
	    });
	    
	    //除当前之外关闭所有
	    $("#m-closeother").click(function(){
	        var currTab = $('#tabs').tabs('getSelected');
	        currTitle = currTab.panel('options').title;    
	        
	        $(".tabs li").each(function(i, n){
	            var title = $(n).text();
	            
	            if(currTitle != title){
	                $('#tabs').tabs('close',title);            
	            }
	        });
	    });
	    
	    //关闭当前
	    $("#m-close").click(function(){
	        var currTab = $('#tabs').tabs('getSelected');
	        currTitle = currTab.panel('options').title;    
	        $('#tabs').tabs('close', currTitle);
	    });
	}); 
  </script>
</html>
