<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../inc/include.jsp" %>
<c:set var="model" value="${requestScope.model}"/>
<c:set var="action" value="${requestScope.action}"/>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<title>${title}</title>
    <jsp:include page="../../inc/Common.jsp"/>
	<script type="text/javascript" src="${thisPath}/js/Common.js?v=${version}"></script>
	<script type="text/javascript" src="${thisPath}/module/js/Pager.js?v=${version}"></script>
	<script>
		Pager.config({
			actionPrefix:'${thisPath}/supplier/',
			title:'客户',
			action:'${action}'
		});
		Pager.init();
		Pager.initForm();
	</script>
</head>
<body>
       <div class="editBox" data-options="region:'north'" style="padding:10px;">
		<form id="editForm" method="post">   
			<input type="hidden" name="model.id" value="${model.id}"/>
	        <ul>
			    <li>   
			        <span class="label" for="username">供应商名称:</span>   
			        <input class="easyui-validatebox" style="width:380px" type="text" name="model.name" data-options="required:true" value="${model.name}"/>   
			    </li>
			    <li>   
			        <span class="label" for="password">联系人:</span>   
			        <input class="easyui-validatebox" type="text" name="model.linkman" data-options="" value="${model.linkman}"/>   
			    </li>
			    <li>   
			        <span class="label" for="password">地址:</span>   
			        <input class="easyui-validatebox" style="width:380px" type="text" name="model.address" data-options="" value="${model.address}"/>   
			    </li> 
			    <li>   
			        <span class="label" for="username">电话:</span>   
			        <input class="easyui-validatebox" type="text" name="model.tel" data-options="" value="${model.tel}"/>   
			    </li>
			    <li>   
			        <span class="label" for="password">邮编:</span>   
			        <input class="easyui-validatebox" type="text" name="model.postcode" data-options="" value="${model.postcode}"/>   
			    </li>
			    <li>   
			        <span class="label" for="password">传真:</span>   
			        <input class="easyui-validatebox" type="text" name="model.fax" data-options="" value="${model.fax}"/>   
			    </li> 
			</ul>   	
		</form>  
	  </div>
</body>
</html>