<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../inc/include.jsp" %>
<c:set var="title" value="用户管理"/>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<title>${title}</title>
    <jsp:include page="../../inc/Common.jsp"/>
	<script type="text/javascript" src="${thisPath}/js/Common.js?v=${version}"></script>
	<script type="text/javascript" src="${thisPath}/module/js/Pager.js?v=${version}"></script>
	<script type="text/javascript">
		Pager.actionPrefix='${thisPath}/user/';
		Pager.title='用户';
		Pager.init();
		Pager.formatEmployee=function(value,row,index){
			return row.employee?row.employee.name:value;
		}
	</script>
</head>
<body>
	<table title="${title}" id="list" width="100%" height="auto" data-options="iconCls:'icon-edit',rownumbers: true,singleSelect:true,idField:'id',fitColumns:true,toolbar:'#tb',onClickRow: Pager.onClickRow">
		<thead>
			<tr>
				<th data-options="field:'username',width:100,editor:'text'">用户名</th>
				<th data-options="field:'employee.name',width:100,editor:'text',formatter:Pager.formatEmployee">员工名称</th>
				<th data-options="field:'state',width:100,editor:'text'">状态</th>
			</tr>
		</thead>
	</table>
	<!-- 工具条及搜索框 -->
	<div id="tb" style="padding:5px;height:auto">
        <div style="margin-bottom:5px;border-bottom:2px dashed #95B8E7;">
        	<a href="#" onclick="Pager.view()" class="easyui-linkbutton" iconCls="icon-ok" plain="true">查看</a>|
            <a href="#" onclick="Pager.append({width:500})" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a>|
            <a href="#" onclick="Pager.edit()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">编辑</a>|
            <a href="#" onclick="Pager.remove()" class="easyui-linkbutton" iconCls="icon-cancel" plain="true">删除</a>|
            <a href="#" onclick="Pager.undo()" class="easyui-linkbutton" iconCls="icon-undo" plain="true">撤销选中</a>
        </div>
        <div>
          <form id="searchForm" method="post">
            用户名: <input name="username" class="textbox" style="width:80px">
              状态:
            <select style="width:100px" class="easyui-combobox" name="state">
            	<option value=""></option>
		        <option value="1">男</option>
		        <option value="2">女</option>
		    </select> 
            </form>
        </div>
    </div>
    <div id="Dialog_"></div>
</body>
</html>