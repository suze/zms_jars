<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../inc/include.jsp" %>
<c:set var="title" value="员工管理"/>
<c:set var="model" value="${requestScope.model}"/>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<title>${title}</title>
    <jsp:include page="../../inc/Common.jsp"/>
	<script type="text/javascript" src="${thisPath}/js/Common.js?v=${version}"></script>
	<script type="text/javascript" src="${thisPath}/module/js/Pager.js?v=${version}"></script>
	<script>
		Pager.actionPrefix='${thisPath}/employee/';
	</script>
</head>
<body>
  <div class="editBox" data-options="region:'north'" style="padding:10px;">
	<form id="editForm" method="post">   
		<input type="hidden" name="model.id" value="${model.id}"/>   
        <ul>
		    <li>   
		        <span class="label" for="name">员工姓名:</span>   
		        <input class="easyui-validatebox" type="text" name="model.name" data-options="required:true" value="${model.name}"/>   
		    </li> 
		    <li>   
		        <span class="label" for="dutyId">职务:</span>   
		        <input class="easyui-combobox" type="text" name="model.dutyId" data-options="
		        required:true,
		        url:'${thisPath}/common/dutyList.do',
	                  valueField:'id',
	                  textField:'name',
	                  panelHeight:'auto'"  value="${model.dutyId}"/>   
		    </li>  
		    <li>   
		        <span class="label" for="gender">性别:</span>   
		        <select class="easyui-combobox" name="model.gender" style="width:152px;">
			        <option selected="selected" value="1">男</option>
			        <option value="2">女</option>
			    </select>
		    </li>
		    <li>   
		        <span class="label" for="birthday">出生日期:</span>   
		        <input class="easyui-datebox" type="text" name="model.birthday" value="${model.birthday}"/>   
		    </li> 
		    <li>   
		        <span class="label" for="firedate">入职日期:</span>   
		        <input class="easyui-datebox" type="text" name="model.firedate" value="${model.firedate}"/>   
		    </li>  
		    <li>   
		        <span class="label" for="idcard">身份证号:</span>   
		        <input class="easyui-validatebox" type="text" name="model.idcard" value="${model.idcard}"/>   
		    </li> 
		    <li>   
		        <span class="label" for="address">住址:</span>
		        <input class="easyui-validatebox" type="text" name="model.address" value="${model.address}" />   
		    </li>  
		    <li>   
		        <span class="label" for="phone">电话:</span>   
		        <input class="easyui-validatebox" type="text" name="model.phone" value="${model.phone}"/>   
		    </li>   
		    <li>   
		        <span class="label" for="email">邮箱:</span>   
		        <input class="easyui-validatebox" type="text" name="model.email" data-options="validType:'email'"  value="${model.email}"/>   
		    </li>
		</ul>   	
	</form>  
  </div>
</body>
</html>