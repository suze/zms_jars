<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../inc/include.jsp" %>
<c:set var="title" value="员工管理"/>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<title>${title}</title>
    <jsp:include page="../../inc/Common.jsp"/>
	<script type="text/javascript" src="${thisPath}/js/Common.js?v=${version}"></script>
	<script type="text/javascript" src="${thisPath}/module/js/Pager.js?v=${version}"></script>
	<script>
		Pager.actionPrefix='${thisPath}/employee/';
		Pager.title='员工';
		Pager.init=function(){
			$(function(){
				$('#list').datagrid({
				url:Pager.actionPrefix+'init.do',
				pageNumber: 1,
				pageSize: 10,//每页显示的记录条数，默认为10
				pageList: [10],//可以设置每页记录条数的列表  
				pagination: {
					pageNumber: 1,
			        beforePageText: '第',//页数文本框前显示的汉字  
			        afterPageText: '页    共 {pages} 页',  
			        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'
				}
				});
			});
		}
		Pager.init();
		Pager.select=function(){
			var row = this.getSelected();
			parent.$('#employeeId').val(row.id);
			parent.$('#employeeName').val(row.name);
			parent.$('#selectDialog').hide();
			parent.$('#selectDialog').window('close');
		}
	</script>
</head>
<body>
	<table title="${title}" id="list" width="80%" height="auto" data-options="iconCls:'icon-edit',rownumbers: true,singleSelect:true,idField:'id',fitColumns:true,toolbar:'#tb',onClickRow: Pager.onClickRow">
		<thead>
			<tr>
				<th data-options="field:'name',width:100,editor:'text'">姓名</th>
				<th data-options="field:'viewDuty',width:100,editor:'text'">职务</th>
			</tr>
		</thead>
	</table>
	<!-- 工具条及搜索框 -->
	<div id="tb" style="padding:5px;height:auto">
        <div>
          <form id="searchForm" method="post">
            姓名: <input name="name" class="textbox" style="width:80px">
            <a href="#" onclick="Pager.query()" class="easyui-linkbutton" iconCls="icon-search">查询</a>
            <a href="#" onclick="Pager.reset()" class="easyui-linkbutton" iconCls="icon-redo">重置</a>
            <a href="#" onclick="Pager.select()" class="easyui-linkbutton" iconCls="icon-ok">选择</a>
            </form>
        </div>
    </div>
    <div id="Dialog_"></div>
</body>
</html>