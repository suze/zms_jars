<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../inc/include.jsp" %>
<c:set var="title" value="菜单管理"/>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<title>${title}</title>
    <jsp:include page="../../inc/Common.jsp"/>
	<script type="text/javascript" src="${thisPath}/js/Common.js"></script>
	<script type="text/javascript" src="${thisPath}/module/js/Tg.js"></script>
	<script type="text/javascript">
	Tg.actionPrefix='${thisPath}/menu/'
	</script>
</head>
<body>
	<table title="${title}" class="easyui-treegrid" id="list" height="auto" data-options="
				iconCls: 'icon-ok',
				rownumbers: true,
				animate: true,
				collapsible: true,
				fitColumns: true,
				idField: 'id',
				treeField: 'name',
				showFooter: true,
				url:'${thisPath}/menu/init.do',
				onContextMenu: Tg.onContextMenu,
				onClickRow: Tg.onClickRow,
				toolbar:[{
					text:'保存',
					iconCls: 'icon-save',
					handler: function(){Tg.save();}
				},'-',{
					text:'刷新',
					iconCls: 'icon-reload',
					handler: function(){Tg.refresh();}
				}]
				">
		<thead>
			<tr>
				<th data-options="field:'name',width:100,editor:'text'">菜单名称</th>
				<th data-options="field:'url',width:300,editor:'text'">菜单地址</th>
				<th data-options="field:'orderNum',width:100,editor:'numberbox'">菜单序号</th>
			</tr>
		</thead>
	</table>
	<div id="mm" class="easyui-menu" style="width:120px;">
        <div onclick="Tg.append({id:'0',name:'新菜单...'})" data-options="iconCls:'icon-add'">添加</div>
        <div onclick="Tg.edit()" data-options="iconCls:'icon-add'">编辑</div>
        <div onclick="Tg.remove()" data-options="iconCls:'icon-remove'">删除</div>
        <div class="menu-sep"></div>
        <div onclick="Tg.collapse()">收缩</div>
        <div onclick="Tg.expand()">展开</div>
    </div>
</body>
</html>