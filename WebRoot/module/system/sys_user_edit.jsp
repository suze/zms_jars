<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../inc/include.jsp" %>
<c:set var="model" value="${requestScope.model}"/>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<title>${title}</title>
    <jsp:include page="../../inc/Common.jsp"/>
	<script type="text/javascript" src="${thisPath}/js/Common.js?v=${version}"></script>
	<script type="text/javascript" src="${thisPath}/module/js/Pager.js?v=${version}"></script>
	<script>
		Pager.actionPrefix='${thisPath}/employee/';
	</script>
</head>
<body>
       <div class="editBox" data-options="region:'north'" style="padding:10px;">
		<form id="editForm" method="post">   
			<input type="hidden" name="model.id" value="${model.id}"/>
			<input id="employeeId" type="hidden" name="model.employeeId" value="${model.employeeId}"/>     
	        <ul>
			    <li>   
			        <span class="label" for="username">用户名:</span>   
			        <input class="easyui-validatebox" type="text" name="model.username" data-options="required:true" value="${model.username}"/>   
			    </li>
			    <li>   
			        <span class="label" for="password">密码:</span>   
			        <input class="easyui-validatebox" type="password" name="model.password" data-options="required:true" value="${model.password}"/>   
			    </li> 
			    <li>  
			    	<span class="label" for="employee">关联员工:</span>    
			        <input id="employeeName"  disabled="disabled"readonly="readonly" class="easyui-validatebox" type="text" name="model.employee.name" value="${model.employee.name}"/>   
			    	<span onclick="Pager.choiceEmp()" class="label choice"><img src="${thisPath}/plugins/jquery.easyui.1.3.6/themes/icons/search.png">选择员工</span>
			    </li>
			</ul>   	
		</form>  
	  </div>
	  <div id="selectDialog" style="display: none;">
	  	<iframe id="iframe" frameborder="0" width="490" height="400" src=""></iframe>
	  </div>      
	 <script type="text/javascript">
	 Pager.choiceEmp=function(){
	 	$('#iframe').attr('src','${thisPath}/module/system/sys_employee_select.jsp');
	 	$('#selectDialog').show();
		$('#selectDialog').window({
		    title: '关联员工',   
		    width: 500,   
		    top:0, 
		    maximizable:true,
		    modal: true,
		    onClose:function(){$('#selectDialog').hide();},
		    onBeforeOpen:function(){$('#selectDialog').show();}
		}); 
	 }
	 </script> 
</body>
</html>