<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../inc/include.jsp" %>
<c:set var="title" value="员工管理"/>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<title>${title}</title>
    <jsp:include page="../../inc/Common.jsp"/>
	<script type="text/javascript" src="${thisPath}/js/Common.js?v=${version}"></script>
	<script type="text/javascript" src="${thisPath}/module/js/Pager.js?v=${version}"></script>
	<script>
		Pager.actionPrefix='${thisPath}/employee/';
		Pager.title='员工';
		Pager.init();
		Pager.formatLink=function(value,row,index){
			return row.duty?row.duty.name:value;
		}
	</script>
</head>
<body>
	<table title="${title}" id="list" width="100%" height="auto" data-options="iconCls:'icon-edit',rownumbers: true,singleSelect:true,idField:'id',fitColumns:true,toolbar:'#tb',onClickRow: Pager.onClickRow">
		<thead>
			<tr>
				<th data-options="field:'name',width:100,editor:'text'">姓名</th>
				<th data-options="field:'duty',width:100,editor:'text',formatter:Pager.formatLink">职务</th>
				<th data-options="field:'gender',width:100,editor:'text',formatter:function(value,row,index){return value=='1'?'男':'女'}">性别</th>
				<th data-options="field:'firedate',width:100,editor:'text',formatter:function(value,row,index){return value?value.substring(0,10):'';}">入职日期</th>
				<th data-options="field:'address',width:100,editor:'text'">住址</th>
				<th data-options="field:'email',width:100,editor:'text'">邮箱</th>
				<th data-options="field:'phone',width:100,editor:'text'">电话</th>
			</tr>
		</thead>
	</table>
	<!-- 工具条及搜索框 -->
	<div id="tb" style="padding:5px;height:auto">
        <div style="margin-bottom:5px;border-bottom:2px dashed #95B8E7;">
        	<a href="#" onclick="Pager.view()" class="easyui-linkbutton" iconCls="icon-ok" plain="true">查看</a>|
            <a href="#" onclick="Pager.append()" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a>|
            <a href="#" onclick="Pager.edit()" class="easyui-linkbutton" iconCls="icon-edit" plain="true">编辑</a>|
            <a href="#" onclick="Pager.remove()" class="easyui-linkbutton" iconCls="icon-cancel" plain="true">删除</a>|
            <a href="#" onclick="Pager.undo()" class="easyui-linkbutton" iconCls="icon-undo" plain="true">撤销选中</a>
        </div>
        <div>
          <form id="searchForm" method="post">
            姓名: <input name="name" class="textbox" style="width:80px">
            职务:
            <input style="width:100px" class="easyui-combobox" name="dutyId" data-options="
		        url:'${thisPath}/common/init.do?type=duty',
                valueField:'id',
                textField:'name',
                panelHeight:'auto'
                "/> 
              性别:
            <select style="width:100px" class="easyui-combobox" name="gender">
            	<option value=""></option>
		        <option value="1">男</option>
		        <option value="2">女</option>
		    </select> 
            入职日期: <input name="firedate" class="easyui-datebox" style="width:80px">
            <a href="#" onclick="Pager.query()" class="easyui-linkbutton" iconCls="icon-search">查询</a>
            <a href="#" onclick="Pager.reset()" class="easyui-linkbutton" iconCls="icon-redo">重置</a>
            </form>
        </div>
    </div>
    <div id="Dialog_"></div>
</body>
</html>