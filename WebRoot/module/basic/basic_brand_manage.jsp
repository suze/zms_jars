<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../inc/include.jsp" %>
<c:set var="title" value="商品品牌"/>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<title>${title}</title>
    <jsp:include page="../../inc/Common.jsp"/>
	<script type="text/javascript" src="${thisPath}/js/Common.js"></script>
	<script type="text/javascript" src="${thisPath}/module/js/Basic.js"></script>
	<script>
		Basic.actionPrefix='${thisPath}/brand/';
		Basic.init();
	</script>
</head>
<body>
	<table title="${title}"id="list" width="100%" height="auto" data-options="iconCls:'icon-edit',singleSelect:true,idField:'id',fitColumns:true,onClickRow: Basic.onClickRow">
		<thead>
			<tr>
				<th data-options="field:'name',width:100,editor:'text'">品牌名称</th>
				<th data-options="field:'orderNum',width:100,editor:'numberbox'">品牌序号</th>
			</tr>
		</thead>
	</table>
</body>
</html>