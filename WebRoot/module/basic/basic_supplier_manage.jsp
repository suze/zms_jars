<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="../../inc/include.jsp" %>
<c:set var="title" value="供应商管理"/>
<html>
  <head>
  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  	<title>${title}</title>
    <jsp:include page="../../inc/Common.jsp"/>
	<script type="text/javascript" src="${thisPath}/js/Common.js?v=${version}"></script>
	<script type="text/javascript" src="${thisPath}/module/js/Pager.js?v=${version}"></script>
	<script type="text/javascript">
		Pager.config({
			actionPrefix:'${thisPath}/supplier/',
			title:'客户',
			action:'${action}'
		});
		Pager.init();
	</script>
</head>
<body>
	<table title="${title}" id="list" width="100%" height="auto" data-options="iconCls:'icon-edit',rownumbers: true,singleSelect:true,idField:'id',fitColumns:true,toolbar:'#tb',onClickRow: Pager.onClickRow">
		<thead>
			<tr>
				<th data-options="field:'name',width:100,editor:'text'">供应商姓名</th>
				<th data-options="field:'tel',width:100,editor:'text'">电话</th>
				<th data-options="field:'address',width:100,editor:'text'">供应商地址</th>
				<th data-options="field:'linkman',width:100,editor:'text'">联系人</th>
				<th data-options="field:'postcode',width:100,editor:'text'">邮编</th>
			</tr>
		</thead>
	</table>
	<!-- 工具条及搜索框 -->
	<div id="tb" style="padding:5px;height:auto">
        <div style="margin-bottom:5px;border-bottom:2px dashed #95B8E7;">
        	<a href="#" onclick="Pager.view({width:600})" class="easyui-linkbutton" iconCls="icon-ok" plain="true">查看</a>|
            <a href="#" onclick="Pager.append({width:600})" class="easyui-linkbutton" iconCls="icon-add" plain="true">新增</a>|
            <a href="#" onclick="Pager.edit({width:600})" class="easyui-linkbutton" iconCls="icon-edit" plain="true">编辑</a>|
            <a href="#" onclick="Pager.remove()" class="easyui-linkbutton" iconCls="icon-cancel" plain="true">删除</a>|
            <a href="#" onclick="Pager.undo()" class="easyui-linkbutton" iconCls="icon-undo" plain="true">撤销选中</a>
        </div>
        <div>
          <form id="searchForm" method="post">
            客户姓名: <input name="name" class="textbox" style="width:80px">
            联系人: <input name="linkman" class="textbox" style="width:80px">
            </form>
        </div>
    </div>
    <div id="Dialog_"></div>
</body>
</html>