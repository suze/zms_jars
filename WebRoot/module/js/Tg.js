/**
 * 本类为Treegrid提供基础功能
 * @description 引用该页面的 table id必须为list
 * @author Chris Suk
 * @date 2014/4/26 22:50
 * @type 
 */
var Tg={
	actionPrefix:'',
	/**
	 * 返回当前选中的节点
	 */
	getSelected:function(){
		return $('#list').treegrid('getSelected');
	},
	/**
	 * 右键菜单
	 * @param {} e
	 * @param {} row
	 */
	onContextMenu:function(e,row){
		e.preventDefault();
	    $(this).treegrid('select', row.id);
	    $('#mm').menu('show',{
	        left: e.pageX,
	        top: e.pageY
	    });
	},
	/**
	 * 收缩
	 */
	collapse:function(){
		var node = Tg.getSelected();
	    if (node){
	        $('#list').treegrid('collapse', node.id);
	    }
	},
	/**
	 * 展开
	 */
	expand:function(){
		var node = Tg.getSelected();
	    if (node){
	        $('#list').treegrid('expand', node.id);
	    }
	},
	/**
	 * treegrid单击触发
	 * @param {} index
	 */
	onClickRow:function(index){
		var node = Tg.getSelected();
		if (node){
             $('#list').treegrid('selectRow', index).treegrid('beginEdit', node.id);
         }
	},
	/**
	 * 添加
	 * @param {} data
	 * @return {}
	 */
	append:function(data){
		var node = Tg.getSelected();
	    $('#list').treegrid('append',{
	        parent: node.id,
	        data: [data]
	    });
	    $('#list').treegrid('beginEdit', data.id);
	},
	/**
	 * 编辑
	 */
	edit:function(){
		var row = $('#list').treegrid('getSelected');
        if (row){
            $('#list').treegrid('beginEdit', row.id);
        }
	},
	/**
	 * 删除
	 */
	remove:function(){
		var node = Tg.getSelected();
	    if (node){
	        $('#list').treegrid('remove', node.id);
	        var obj={id:node.id};
	         $.post(Tg.actionPrefix+'delete.do',obj,function(){
		     	$.msg('删除成功');
			    $('#list').treegrid('reload');
		     });
	    }
	},
	/**
	 * 保存
	 */
	save:function(){
		var obj = Tg.getSelected();
		var e = $('#list').treegrid('find',obj.id);
	     $.post(Tg.actionPrefix+'save.do',obj,function(){
	     	$.msg('保存成功');
		    $('#list').treegrid('reload');
	     });
	},
	/**
	 * 刷新
	 */
	refresh:function(){
		$('#list').treegrid('reload');
	}
}