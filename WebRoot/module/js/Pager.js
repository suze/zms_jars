/**
 * 本基类为基础数据模块提供公共服务业务处理
 * @author Chris Suk
 * @date 2014/4/26
 * @type 
 */
var Pager={
	/**
	 * 全局参数
	 * @type 
	 */
	params:{
		/**
		 * action前缀
		 * @type String
		 */
		actionPrefix:'',
		title:'',
		action:''
	},
	/**
	 * 配置参数
	 * @param {} args
	 */
	config:function(args){
		this.params=$.extend(this.params,args);
	},
	/**
	 * 初始化配置
	 */
	init:function(){
		$(function(){
			$('#editWindow').window('close');
			$('#list').datagrid({
			url:Pager.params.actionPrefix+'init.do',
			pageNumber: 1,
			pageSize: 15,//每页显示的记录条数，默认为10
			pageList: [15,30,50],//可以设置每页记录条数的列表  
			pagination: {
				pageNumber: 1,
		        beforePageText: '第',//页数文本框前显示的汉字  
		        afterPageText: '页    共 {pages} 页',  
		        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'
			}
			});
		});
	},
	initForm:function(){
		alert(JSON.stringify(this.params));
		/**
		 * 如果是查看，则设置表单为只读状态
		 */
		if(this.params.action=='view'){
			this.readOnly();
		}
	},
	/**
	 * 使表单处于只读状态
	 */
	readOnly:function(){
		$('#editForm input').attr('disabled','disabled');
	},
	/**
	 * 撤销选中
	 */
	undo:function(){
		$('#list').datagrid('unselectAll');
	},
	/**
	 * 获取选中记录
	 * @return {}
	 */
	getSelected:function(){
		 return $('#list').datagrid('getSelected');
	},
	/**
	 * Datagrid单击触发
	 * @param {} index
	 */
	onClickRow:function(index){
         $('#list').datagrid('selectRow', index);
	},
	/**
	 * 新增
	 */
	append:function(args){
		var opts = {
				width: 400
		};
		opts=$.extend(opts,args);
		$.messager.progress({text:'数据加载中....',interval:100});
		$('#Dialog_').dialog({
		    title: '新增-'+this.params.title,   
		    width: opts.width,   
		    top:0, 
		    cache: false,
		    maximizable:true,
		    href: this.params.actionPrefix+'edit.do',   
		    modal: true,
		    onLoad:function(){$.messager.progress('close');},
			buttons:[{
					text:"保存",
					iconCls:'icon-save',
					handler:function() {
						Pager.save();
					}
				}, {
					text:"关闭",
					iconCls:'icon-cancel',
					handler:function() {
						$('#Dialog_').dialog('close');
					}
			}]
		});   
	},
	/**
	 * 编辑
	 */
	edit:function(args){
		var row = this.getSelected();
		if(row){
			var opts = {
				width: 400
			};
			opts=$.extend(opts,args);
			$.messager.progress({text:'数据加载中....',interval:100});
			$('#Dialog_').dialog({
			    title: '编辑-'+this.params.title,   
			    width: opts.width,   
			    top:0, 
			    cache: false,
			    maximizable:true,
			    href: this.params.actionPrefix+'edit.do?id='+row.id,   
			    modal: true,
			    onLoad:function(){$.messager.progress('close');},
				buttons:[ {
					text:"保存",
					iconCls:'icon-save',
					handler:function() {
						Pager.save();
					}
				}, {
					text:"关闭",
					iconCls:'icon-cancel',
					handler:function() {
						$('#Dialog_').dialog('close');
					}
				}]
			});   
		}else{
			$.msg('请选中一条记录');
		}
	},
	/**
	 * 查看
	 */
	view:function(args){
		var row = this.getSelected();
		var opts = {
			width: 400
		};
		opts=$.extend(opts,args);
		if(!row){
			$.msg('请选中一条记录');return false;
		}
		$.messager.progress({text:'数据加载中....',interval:100});
		$('#Dialog_').dialog({
		    title: '查看-'+this.params.title,   
		    width: opts.width,   
		    top:0, 
		    cache: false,
		    maximizable:true,
		    href:this.params.actionPrefix+'edit.do?action=view&id='+row.id,   
		    modal: true,
		    onLoad:function(){$.messager.progress('close');},
			buttons:[ {
				text:"关闭",
				iconCls:'icon-cancel',
				handler:function() {
					$('#Dialog_').dialog('close');
				}
			}]
		});   
	},
	/**
	 * 删除
	 */
	remove:function(){
		$.messager.confirm('操作提示', '您确定要删除选中记录吗？', function(r){
			if (r){
			  var rows = $('#list').datagrid('getSelected');
			  if(rows){
			      var ids = rows.id;
			      $.post(this.params.actionPrefix+'delete.do',{ids:ids},function(){
					 $('#list').datagrid('reload');
				     $.msg('删除成功');
				  });
			  }else{
			  		$.msg('请选中一条记录');
			  }
			}
		});
	},
	/**
	 * 保存
	 */
	save:function(){
		$.messager.progress();
		var isValid = $('#editForm').form('validate');
		if (!isValid){
			$.messager.progress('close');	// 如果表单是无效的则隐藏进度条
		}else{
			 var param = $('#editForm').serialize();
		     $.post(this.params.actionPrefix+'save.do',param,function(result){
		     	result=eval('('+result+')');
		     	if(result.type==0){
			     	$.msg(result.message);
				    $('#list').datagrid('reload');
		     	}else{
		     		$.msg(result.message);
		     	}
			    $.messager.progress('close');	// 如果提交成功则隐藏进度条
			    $('#Dialog_').dialog('close');
		     });
		}
	},
	/**
	 * 高级搜索
	 */
	query:function(){
		var param=$.serializeObject($('#searchForm'));
		$('#list').datagrid('load',param);
	},
	/**
	 * 重置搜索表单
	 */
	reset:function(){
		$('#searchForm')[0].reset();
		$('#list').datagrid('load', {});
	}
}