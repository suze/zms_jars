var editIndex = undefined;
/**
 * 本基类为基础数据模块提供公共服务业务处理
 * @author Chris Suk
 * @date 2014/4/26
 * @type 
 */
var Basic={
	/**
	 * action前缀
	 * @type String
	 */
	actionPrefix:'',
	/**
	 * 初始化配置
	 */
	init:function(){
		$(function(){
			$('#list').datagrid({
			url:Basic.actionPrefix+'init.do',
			toolbar:[{
				text:'新增',
				iconCls: 'icon-add',
				handler: function(){Basic.append();}
			},'-',{
				text:'删除',
				iconCls: 'icon-remove',
				handler: function(){Basic.remove();}
			},'-',{
				text:'保存',
				iconCls: 'icon-save',
				handler: function(){Basic.save();}
			}],
			pagination: true
			});
		});
	},
	/**
	 * Datagrid单击触发
	 * @param {} index
	 */
	onClickRow:function(index){
		if (editIndex != index){
         	 Basic.endEditing();
             $('#list').datagrid('selectRow', index).datagrid('beginEdit', index);
             editIndex = index;
         }
	},
	/**
	 * 判断是否正在编辑
	 */
	endEditing:function(){
		var rows = $('#list').datagrid('getRows');
        for ( var i = 0; i < rows.length; i++) {
               $('#list').datagrid('endEdit', i);
           }
	},
	/**
	 * 新增
	 */
	append:function(){
		 Basic.endEditing();
        $('#list').datagrid('appendRow',{status:'P'});
        editIndex = $('#list').datagrid('getRows').length-1;
        $('#list').datagrid('selectRow', editIndex).datagrid('beginEdit', editIndex);
	},
	/**
	 * 删除
	 */
	remove:function(){
		$.messager.confirm('操作提示', '您确定要删除选中记录吗？', function(r){
			if (r){
			  var rows = $('#list').datagrid('getSelected');
			  if(rows){
			      var ids = rows.id;
			      $.post(Basic.actionPrefix+'delete.do',{ids:ids},function(){
					 $('#list').datagrid('reload');
				     $.msg('删除成功');
				  });
			  }else{
			  		$.msg('请选中一条记录');
			  }
			}
		});
	},
	/**
	 * 保存
	 */
	save:function(){
		 Basic.endEditing();
	     var obj = $('#list').datagrid('getChanges')[0];
	     $.post(Basic.actionPrefix+'save.do',obj,function(){
	     	$.msg('保存成功');
		    $('#list').datagrid('reload');
	     });
	}
}