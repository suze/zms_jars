<%@ page language="java" isThreadSafe="true" contentType="text/html;charset=UTF-8"%>
<%@ include file="inc/include.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>${appTitle}</title>
		<link rel="stylesheet" type="text/css" href="css/login.css?v=${version}" />
		<script type="text/javascript" src="plugins/jquery1.8.js"></script>
		<script type="text/javascript" src="plugins/jquery.poshytip/jquery.poshytip.js"></script>
		<link rel="stylesheet" type="text/css" href="plugins/jquery.poshytip/css/css.css">
		<script type="text/javascript" src="js/Common.js?v=${version}"></script>
		<script type="text/javascript" src="js/login.js?v=${version}"></script>
	</head>
	<body>
		<div style="margin: 0 auto; width: 980px; margin-top: 100px;" id="loginDiv">
			<form  onkeypress="if(event.keyCode==13){Login.check();}" action="" method="post" class="validate" id="loginForm">
				<div class="fl">
					<img src="css/images/login.jpg">
				</div>
				<div class="loginpanel loginpanel-novcode fr">
					<div class="inner">
						<div class="panelbody">
							<div class="header">
								<h3>
									&nbsp;&nbsp;系统登录
								</h3>
							</div>
							<div class="body">
								<div class="row">
									<div class="caption">
										用户名
									</div>
									<div class="content">
										<div class="textbox textbox-tip">
											<input autocomplete="off" id="username" required="true" tip="用户名" type="text" name="username" value="">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="caption">
										密 码
									</div>
									<div class="content">
										<div class="textbox textbox-tip">
											<input id="password" type="password" required="true" tip="密码" name="password" value="">
										</div>
									</div>
								</div>
								<div class="row buttons ">
									<button type="submit" id="subtn" style="display: none;"></button>
									<a href="javascript:void(0);" onclick="Login.check()" id="btn_login" class="loginbtn"></a><a href="javascript:void(0);">忘记密码？</a>
								</div>
								<div id="msg" class="row buttons pt5 pb5 pr0 msg">
								</div>
								<div id="msg" class="row buttons pt5 pb5 pr0">
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>