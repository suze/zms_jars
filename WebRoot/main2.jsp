<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="inc/include.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  	<title>欢迎登录ZMS(月光石)系统</title>
    <link rel="stylesheet" type="text/css" href="${thisPath}/plugins/jquery.easyui.1.3.1/themes/default/easyui.css">
	<script type="text/javascript" src="${thisPath}/plugins/jquery.easyui.1.3.1/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="${thisPath}/plugins/jquery.easyui.1.3.1/jquery.easyui.min.js"></script>
	<link rel="stylesheet" href="${thisPath}/plugins/jquery.ztree.3.5/css/demo.css" type="text/css">
	<link rel="stylesheet" href="${thisPath}/plugins/jquery.ztree.3.5/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="${thisPath}/plugins/jquery.ztree.3.5/jquery.ztree.core-3.5.js"></script>
  </head>
  
  <body class="easyui-layout">
	<div id="top" data-options="region:'north',split:true,collapsed:true,border:false" style="height:60px;background:#B3DFDA;padding:10px">欢迎登录(ZMS)系统</div>
	<div id="left" data-options="region:'west',split:true,title:'系统菜单'" style="width:230px;padding:10px;"><ul id="menu" class="ztree"></ul></div>
	<div id="right" data-options="region:'east',split:true,collapsed:true,title:'系统消息'" style="width:100px;padding:10px;">east region</div>
	<div id="bottom" data-options="region:'south',split:true,collapsed:true,border:false" style="height:50px;background:#A9FACD;padding:10px;">版权所有&copy;Chris Suk</div>
	<div region='center' id="content">
		<div id="Maintabs" class="easyui-tabs">
        	<div title="主页面" href="index.jsp" closable=true></div> 
    	</div>
	</div>
  </body>
  <script type="text/javascript">
  	var ztree;
 	var setting = {
 		data: {
 			key:{
 				url:'_url'
 			},
			simpleData: {
				enable: true
			}
		},callback: {
			onClick:function(event, treeId, treeNode){
				var title = treeNode.title;
				if(!$("#Maintabs").tabs('exists',title)){// 如果不存在此tab则创建
			        if(treeNode.url!="")
			            $("#Maintabs").tabs('add',{
			                title:treeNode.name, 
			                href:treeNode.url,
			                closable:true,
			            });
			    }else{// 如果已经打开则选中
			        $("#Maintabs").tabs('select',title);
			    }
			}
		}
	};
	function showTab(id,url){
		alert(url);
	}
	$(function(){
	   $.getJSON('${thisPath}/menu/init.do?v='+new Date(),{id:'0'},function(data){
	    	data=eval(data);
	    	zTree=$.fn.zTree.init($("#menu"), setting, data);
	    });
	}); 
  </script>
</html>
