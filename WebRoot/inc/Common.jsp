<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt"  uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set value="${pageContext.request.contextPath}" var="thisPath"/>
<%String v = java.util.UUID.randomUUID().toString();%>
<c:set var="version" value="<%=v %>"/>
<link rel="stylesheet" type="text/css" href="${thisPath}/plugins/jquery.easyui.1.3.6/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${thisPath}/plugins/jquery.easyui.1.3.6/themes/icon.css">
<link rel="stylesheet" type="text/css" href="${thisPath}/css/Common.css?v=${version}">
<script type="text/javascript" src="${thisPath}/plugins/jquery.easyui.1.3.6/jquery.min.js"></script>
<script type="text/javascript" src="${thisPath}/plugins/jquery.easyui.1.3.6/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${thisPath}/plugins/jquery.easyui.1.3.6/easyui-lang-zh_CN.js"></script>
